import './App.css'
import React from 'react'
import {
  Navigate,
  Route,
  Routes,
  BrowserRouter as Router,
} from 'react-router-dom'
import DelegationDecision from './components/delegationdecision/DelegationDecision'
import Instructions from './components/instructions/Instructions'
import Instructionquestion from './components/instructionquestion/Instructionquestion'
import Homepage from './components/homepage/Homepage'
import TransitionLearning from './components/transitionlearning/TransitionLearning'
import CropModel from './components/cropmodel/CropModel'
import ChartModel from './components/cropmodel/ChartModel'
import Traininground from './components/learninground/Traininground'
import OfficialRound from './components/learninground/Officialround'
import PredictionInput from './components/learninground/test'
import Questionnaire from './components/questionnaire/Questionnaire'
import Error from './components/error/Error'

function App() {
  return (
    <div className='App'>
      <Router>
        <Routes>
          <Route path='/' element={<Homepage />} />
          <Route path='/error' element={<Error />} />
          <Route path='/instructions' element={<Instructions />} />
          <Route path='/crop-model' element={<CropModel />} />
          <Route path='/traininground' element={<Traininground />} />
          <Route path='/test' element={<PredictionInput />} />
          <Route path='/officialround' element={<OfficialRound />} />
          <Route path='/chart-model' element={<ChartModel />} />
          <Route path='/delegationdecision' element={<DelegationDecision />} />
          <Route
            path='/instructionquestion'
            element={<Instructionquestion />}
          />
          {/* <Route path='/learninground' element={<Learninground />} /> */}
          <Route path='/transitionlearning' element={<TransitionLearning />} />
          <Route path='/questionnaire' element={<Questionnaire />} />
          <Route path='*' element={<Navigate to='/' />} />
        </Routes>
      </Router>
    </div>
  )
}

export default App
