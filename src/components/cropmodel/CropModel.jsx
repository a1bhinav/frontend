import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Layout, Button, Typography, Table } from 'antd'

import 'antd/dist/antd.css'
import ChartModel from './ChartModel'

import './CropModel.css'

const { Content } = Layout
const { Title } = Typography

export const meemmaseedbestcolumns = [
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <Title level={5}>Sunshine</Title>
      </div>
    ),
    dataIndex: 'Sunshine',
    key: 'Sunshine',
    align: 'center',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <Title level={5}>Temperature</Title>
      </div>
    ),
    dataIndex: 'Temperature',
    key: 'Temperature',
    align: 'center',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <Title level={5}>Wind</Title>
      </div>
    ),
    dataIndex: 'Wind',
    key: 'Wind',
    align: 'center',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <Title level={5}>Additional Irrigation</Title>
        <div>(between 0-70)</div>
      </div>
    ),
    dataIndex: 'AdditionalIrrigation',
    key: 'AdditionalIrrigation',
    align: 'center',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <Title level={5}>AI System’s Estimation of Additional Irrigation</Title>
        <div>(between 0-70)</div>
      </div>
    ),
    dataIndex: 'ModelEstimation',
    key: 'ModelEstimation',
    align: 'center',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <Title level={5}>AI System Error</Title>
      </div>
    ),
    dataIndex: 'ModelError',
    key: 'ModelError',
    align: 'center',
  },
]



const CropModel = () => {
  const [page, setPage] = useState(0)
  const currentPage = [0, 1, 2]
  const randomtype1 = sessionStorage.getItem('randomtype1');
  const randomtype2 = sessionStorage.getItem('randomtype2');
  const [Meemmaseed, setMeemmaseed] = useState();
  const [Vussanut, setVussanut] = useState();

  const getTreatment = (treatment) => {
    switch (treatment) {
      case 1:
      case '1':
        setMeemmaseed(getMeemmaseedbest());
        setVussanut(getVussanutbest());
        break;
      case 2:
      case "2":
        setMeemmaseed(getMeemmaseedRare(randomtype1));
        setVussanut(getVussanutbest());
        break;
      case 3:
      case '3':
        setMeemmaseed(getMeemmaseedbest());
        setVussanut(getVussanutRare(randomtype2));
        break;
      case 4:
      case '4':
        setMeemmaseed(getMeemmaseedBPRare(randomtype1));
        setVussanut(getVussanutBPRare(randomtype2));
        break;
      case 5:
      case '5':
        setMeemmaseed(getMeemmaseedCont(randomtype1));
        setVussanut(getVussanutBPRare(randomtype2));
        break;
      case 6:
      case '6':
        setMeemmaseed(getMeemmaseedBPRare(randomtype1));
        setVussanut(getVussanutCont(randomtype2));
        break;
    }
  }

  const navigate = useNavigate();

  // useEffect(() => {
  //   if (!Meemmaseed) {
  //     setMeemmaseed(getMeemmaseedbest());
  //   }
  //   if (!Vussanut) {
  //     setVussanut(getVussanutbest());
  //   }
  // })

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          } else {
            const treatment = data.treatment
            getTreatment(treatment);
            sessionStorage.setItem('treatment', treatment)
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <div className='total-screen'>
      <Layout className='layout'>
        <div className='header-style pb0'>
          {page === 0 && (
            <Title level={2}>
              Meemmaseed Irrigation and AI System Information
            </Title>
          )}
          {page === 1 && (
            <Title level={2}>
              Vussanut Irrigation and AI System Information
            </Title>
          )}
          {page === 2 && (
            <Title level={2}>
              AI System Errors Curves Meemmaseed and Vussanut
            </Title>
          )}
        </div>
        {page === 0 && (
          <Title
            level={4}
            className='flex-column-center'
            style={{
              margin: '0 17% 3rem',
              fontWeight: '400',
              fontSize: '1rem',
            }}
          >
            Hint: Meemmaseed is known to be unaffected by different
            temperatures, but very sensitive to wind speed. Therefore, to
            estimate the additional irrigation needed for your Meemmaseed crop,
            you should focus mainly on the third input variable Wind and ignore
            the second environmental variable Temperature.
          </Title>
        )}
        {page === 1 && (
          <Title
            level={4}
            className='flex-column-center'
            style={{ marginBottom: '3rem' }}
          ></Title>
        )}
        {page === 2 && (
          <Title
            level={5}
            className='flex-column-center'
            style={{ marginBottom: '3rem' }}
          >
            The figure below shows the AI System's irrigation error for both
            crops in each round. Higher values refers to a larger irrigation
            error.
          </Title>
        )}
        <Content className='site-layout-content' style={{ paddingTop: 0 }}>
          {page === 0 && (
            <div className='flex-column-center'>
              <Table
                style={{ marginTop: 0 }}
                dataSource={Meemmaseed}
                columns={meemmaseedbestcolumns}
                pagination={false}
                rowClassName='custom-row'
              />
            </div>
          )}
          {page === 1 && (
            <div className='flex-column-center'>
              <Table
                style={{ marginTop: 0 }}
                dataSource={Vussanut}
                columns={meemmaseedbestcolumns}
                pagination={false}
              />
            </div>
          )}
          {page === 2 && (
            <div>
              <p className='card-text flex-column-center'>
                <ChartModel VussanutModelError={Vussanut} MeemmaseedModelError={Meemmaseed} />
              </p>
              <p className='card-text'>
                If you have familiarized yourself with the two crops, please
                continue to the training rounds. The training rounds do not
                affect your bonus. However, they are meant to provide you with
                an opportunity to learn about your crops and learn how to make
                good estimations.
              </p>
              <div className='button-container'>
                <Button type='primary' shape='round' size='large'
                  onClick={async () => {
                    try {
                      const P_ID = await sessionStorage.getItem("P_ID");

                      const res = await fetch("https://human-computer-interaction-backend.onrender.com/api/pushTableData", {
                        method: "POST",
                        headers: {  
                            Accept: "application/json",  
                            "Access-Control-Allow-Origin": "*",  
                            "Access-Control-Allow-Methods": "*",  
                            "Content-Type": "application/json",  
                          },  
                        body: JSON.stringify({
                          P_ID: P_ID, MeemmaseedTable: Meemmaseed, VussanutTable: Vussanut
                        }),
                      //  credentials: "include",
                      });

                      if (!(res.status === 200)) {
                        throw new Error(res.err);
                      } else {
                        navigate("/traininground");
                      }
                    } catch (err) {
                      console.log(err);
                    }
                  }}>
                  Start Training Rounds
                </Button>
              </div>
            </div>
          )}

          <div id='wrapper'>
            <div class='b-pagination-outer'>
              <ul id='border-pagination'>
                <li>
                  <button
                    onClick={() => {
                      if (page !== 0) {
                        setPage(page - 1)
                      }
                    }}
                  >
                    «
                  </button>
                </li>

                {currentPage.map((currentPage) => (
                  <li key={currentPage}>
                    {currentPage === page && (
                      <button
                        className='active'
                        onClick={() => {
                          setPage(currentPage)
                        }}
                      >
                        {currentPage + 1}
                      </button>
                    )}
                    {currentPage !== page && (
                      <button
                        onClick={() => {
                          setPage(currentPage)
                        }}
                      >
                        {currentPage + 1}
                      </button>
                    )}
                  </li>
                ))}
                <li>
                  <button
                    onClick={() => {
                      if (page !== 2) {
                        setPage(page + 1)
                      }
                    }}
                  >
                    »
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </Content>
      </Layout>
    </div>
  )
}

export default CropModel;


const Sunshine = [16, 9, 6, 10, 6, 17, 3, 8, 1, 17, 9, 9, 18, 1, 18, 11, 14, 3, 10, 3];

const Temperature = [99, 39, 87, 34, 49, 96, 86, 50, 60, 90, 108, 83, 67, 65, 95, 47, 43, 35, 94, 51];

const Wind = [60, 40, 6, 59, 35, 39, 58, 16, 55, 14, 55, 14, 5, 41, 24, 30, 50, 35, 61, 49];

export const getVussanutbest = () => {
  const meemmaseedbest = [];

  const AdditionalIrrigation = [38, 8, 46, 8, 17, 49, 26, 20, 18, 50, 41, 38, 34, 24, 52, 22, 7, 14, 37, 10];

  const ModelEstimation = [39, 11, 47, 3, 17, 44, 30, 24, 17, 48, 45, 43, 38, 24, 48, 19, 11, 9, 35, 14];

  const ModelError = [1, 3, 1, 5, 0, 5, 4, 4, 1, 2, 4, 5, 4, 0, 4, 3, 4, 5, 2, 4];

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[i],
      ModelError: ModelError[i],
    })
  }
  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })
}

export const getVussanutRare = (index) => {
  const meemmaseedbest = [];

  const AdditionalIrrigation = [38, 8, 46, 8, 17, 49, 26, 20, 18, 50, 41, 38, 34, 24, 52, 22, 7, 14, 37, 10];

  const ModelEstimation = [
    [39, 41, 47, -27, -13, 44, 60, 24, 17, 18, 75, 73, 38, 24, 18, 19, 11, 9, 5, 44],
    [69, 11, 77, 3, 47, 44, 60, 24, 17, 18, 75, 43, 68, 24, 18, 19, 11, 9, 5, 44],
    [69, 41, 47, 3, 47, 14, 60, 24, 17, 18, 45, 43, 68, 24, 18, 19, 11, 9, 5, 44],
    [39, 41, 47, -27, -13, 44, 60, 24, -13, 48, 75, 73, 38, 24, 48, -11, 11, 9, 5, 44],
    [39, 11, 77, 3, 47, 74, 60, 24, 17, 18, 75, 73, 38, 24, 18, 19, 11, 9, 5, 44]
  ]

  const ModelError = [
    [1, 34, 1, 35, 30, 5, 34, 4, 1, 32, 34, 35, 4, 0, 34, 3, 4, 5, 32, 34],
    [31, 3, 31, 5, 30, 5, 34, 4, 1, 32, 34, 5, 34, 0, 34, 3, 4, 5, 32, 34],
    [31, 33, 1, 5, 30, 35, 34, 4, 1, 32, 4, 5, 34, 0, 34, 3, 4, 5, 32, 34],
    [1, 34, 1, 35, 30, 5, 34, 4, 31, 2, 34, 35, 4, 0, 4, 33, 4, 5, 32, 34],
    [1, 3, 31, 5, 30, 35, 34, 4, 1, 32, 34, 35, 4, 0, 34, 3, 4, 5, 32, 34]
  ]

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[index - 1][i],
      ModelError: ModelError[index - 1][i],
    })
  }
  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })
}

export const getVussanutBPRare = (index) => {
  const meemmaseedbest = [];

  const AdditionalIrrigation = [39, 11, 47, 3, 17, 44, 30, 24, 17, 48, 45, 43, 38, 24, 48, 19, 11, 9, 35, 14];

  const ModelEstimation = [
    [39, 11, 47, 3, 8, 44, 42, 24, 17, 48, 45, 43, 38, 44, 48, 19, 11, 9, 35, 33],
    [39, 11, 47, 3, 27, 44, 30, 24, 17, 40, 45, 43, 38, 24, 48, 19, 11, 9, 10, 31],
    [60, 11, 47, 22, 17, 44, 30, 24, 17, 48, 45, 43, 27, 24, 48, 19, 11, 9, 35, 5],
    [39, 22, 47, 3, 17, 44, 30, 24, 17, 48, 45, 43, 55, 24, 29, 19, 11, 9, 22, 14],
    [18, 11, 47, 3, 17, 18, 30, 24, 17, 48, 48, 43, 38, 24, 48, 19, 11, 9, 35, 4]
  ]

  const ModelError = [
    [0, 0, 0, 0, 9, 0, 12, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 19],
    [0, 0, 0, 0, 10, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 25, 17],
    [21, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 9],
    [0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 0, 19, 0, 0, 0, 13, 0],
    [21, 0, 0, 0, 0, 26, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 10]
  ]

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[index - 1][i],
      ModelError: ModelError[index - 1][i],
    })
  }
  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })
}

export const getVussanutCont = (index) => {
  const meemmaseedbest = [];

  const AdditionalIrrigation = [39, 11, 47, 3, 17, 44, 30, 24, 17, 48, 45, 43, 38, 24, 48, 19, 11, 9, 35, 14];

  const ModelEstimation = [
    [26, -5, 63, 20, -10, 57, 57, 10, 34, 61, 31, 26, 22, 59, 35, 35, 25, 26, 49, 48],
    [50, 25, 60, 14, 32, 58, 19, 12, 30, 25, 32, 30, 26, 10, 36, 33, 23, 20, 0, 48],
    [74, 24, 63, 38, 3, 61, 17, 10, 3, 61, 59, 26, 12, 8, 32, 6, -6, 25, 18, -10],
    [53, 37, 64, 16, 33, 61, 44, 40, 34, 35, 32, 59, 70, 7, 14, 3, 25, 22, 7, 0],
    [4, -6, 31, 17, 4, 9, 43, 41, 31, 64, 66, 30, 25, 8, 62, 2, -6, 23, 52, -12]
  ]

  const ModelError = [
    [13, 16, 16, 17, 27, 13, 27, 14, 17, 13, 14, 17, 16, 35, 13, 16, 14, 17, 14, 34],
    [11, 14, 13, 11, 25, 14, 11, 12, 13, 23, 13, 13, 12, 14, 12, 14, 12, 11, 35, 32],
    [35, 13, 16, 35, 14, 17, 13, 14, 14, 13, 14, 17, 26, 16, 16, 13, 17, 16, 17, 24],
    [14, 26, 17, 13, 16, 17, 14, 16, 17, 13, 13, 16, 32, 17, 34, 16, 14, 13, 28, 14],
    [35, 17, 16, 14, 13, 35, 13, 17, 14, 16, 21, 13, 13, 16, 14, 16, 17, 14, 17, 26]
  ]

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[index - 1][i],
      ModelError: ModelError[index - 1][i],
    })
  }
  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })
}

export const getMeemmaseedbest = () => {
  const meemmaseedbest = [];

  const AdditionalIrrigation = [52, 34, 4, 59, 33, 39, 54, 15, 55, 10, 47, 10, 2, 32, 28, 31, 42, 36, 56, 43];

  const ModelEstimation = [56, 37, 6, 54, 32, 37, 53, 15, 50, 14, 50, 14, 6, 37, 23, 28, 46, 32, 56, 44];

  const ModelError = [4, 3, 2, 5, 1, 2, 1, 0, 5, 4, 3, 4, 4, 5, 5, 3, 4, 4, 0, 1];

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[i],
      ModelError: ModelError[i],
    })
  }

  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })
}

export const getMeemmaseedRare = (index) => {

  const meemmaseedbest = [];

  const AdditionalIrrigation = [52, 34, 4, 59, 33, 39, 54, 15, 55, 10, 47, 10, 2, 32, 28, 31, 42, 36, 56, 43];

  const ModelEstimation = [
    [56, 61, 6, 30, 8, 37, 53, -9, 26, 38, 50, 38, 6, 37, -1, 28, 46, 8, 56, 68],
    [80, 37, 6, 30, 8, 37, 53, 15, 26, 38, 50, 38, 6, 37, 23, 4, 46, 8, 80, 68],
    [56, 61, 6, 30, 8, 37, 29, 16, 26, 38, 50, 38, 6, 37, 23, 4, 46, 8, 56, 68],
    [56, 61, -22, 54, 8, 37, 53, -9, 26, 38, 50, 14, -26, 37, -1, 28, 46, 8, 56, 68],
    [80, 37, 6, 30, 32, 37, 79, 15, 26, 38, 74, 14, 6, 37, 23, 4, 46, 8, 80, 68]
  ];

  const ModelError = [
    [4, 27, 2, 29, 25, 2, 1, 24, 29, 28, 3, 28, 4, 5, 29, 3, 4, 28, 0, 25],
    [28, 3, 2, 29, 25, 2, 1, 0, 29, 28, 3, 28, 4, 5, 5, 27, 4, 28, 24, 25],
    [4, 27, 2, 29, 25, 2, 25, 1, 29, 28, 3, 28, 4, 5, 5, 27, 4, 28, 0, 25],
    [4, 27, 26, 5, 25, 2, 1, 24, 29, 28, 3, 4, 28, 5, 29, 3, 4, 28, 0, 25],
    [28, 3, 2, 29, 1, 2, 25, 0, 29, 28, 27, 4, 4, 5, 5, 27, 4, 28, 24, 25]
  ];

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[index - 1][i],
      ModelError: ModelError[index - 1][i],
    })
  }

  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })

}

export const getMeemmaseedBPRare = (index) => {

  const meemmaseedbest = [];

  const AdditionalIrrigation = [56, 37, 6, 54, 32, 37, 53, 15, 50, 14, 50, 14, 6, 37, 23, 28, 46, 32, 56, 44];

  const ModelEstimation = [
    [56, 28, 6, 54, 32, 37, 53, 15, 66, 14, 50, 14, 6, 23, 23, 28, 46, 53, 56, 44],
    [56, 37, 29, 54, 32, 37, 53, 15, 50, 14, 50, 14, -4, 37, 23, 7, 46, 32, 62, 44],
    [74, 37, -15, 54, 32, 37, 53, 26, 50, 14, 50, 14, 6, 37, 23, 28, 46, 32, 56, 34],
    [56, 37, 19, 54, 32, 37, 53, 40, 50, 14, 50, 14, 6, 37, 23, 28, 46, 32, 39, 49],
    [56, 37, -19, 54, 32, 37, 53, 15, 50, -2, 50, 14, -8, 37, 23, 28, 51, 32, 56, 44]
  ]

  const ModelError = [
    [0, 9, 0, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 14, 0, 0, 0, 21, 0, 0],
    [0, 0, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 21, 0, 0, 6, 0],
    [18, 0, 21, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10],
    [0, 0, 13, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 5],
    [0, 0, 25, 0, 0, 0, 0, 0, 0, 16, 0, 0, 14, 0, 0, 0, 5, 0, 0, 0]
  ]

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[index - 1][i],
      ModelError: ModelError[index - 1][i],
    })
  }

  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })

}

export const getMeemmaseedCont = (index) => {
  const meemmaseedbest = [];

  const AdditionalIrrigation = [56, 37, 6, 54, 32, 37, 53, 15, 50, 14, 50, 14, 6, 37, 23, 28, 46, 32, 56, 44];

  const ModelEstimation = [
    [45, 16, 16, 68, 19, 24, 43, 29, 88, 3, 61, 4, 20, 1, 36, 38, 59, 65, 45, 30],
    [67, 50, 41, 44, 18, 51, 67, 2, 39, 1, 60, 25, -16, 37, 10, -5, 32, 42, 74, 54],
    [86, 24, -27, 65, 19, 51, 64, 38, 60, 4, 36, 25, 19, 51, 13, 15, 26, 46, 67, 22],
    [70, 48, 31, 40, 45, 17, 40, 52, 39, 3, 40, 1, 16, 27, 10, 14, 60, 21, 27, 61],
    [46, 51, -31, 64, 43, 50, 67, 4, 39, -14, 37, 28, -20, 50, 12, 15, 63, 46, 46, 34]
  ]

  const ModelError = [
    [11, 21, 10, 14, 13, 13, 10, 14, 38, 11, 11, 10, 14, 36, 13, 10, 13, 33, 11, 14],
    [11, 13, 35, 10, 14, 14, 14, 13, 11, 13, 10, 11, 22, 11, 13, 33, 14, 10, 18, 10],
    [30, 13, 33, 11, 13, 14, 11, 23, 10, 10, 14, 11, 13, 14, 10, 13, 10, 14, 11, 22],
    [14, 11, 25, 14, 13, 10, 13, 37, 11, 11, 10, 13, 10, 10, 13, 14, 14, 11, 29, 17],
    [10, 14, 37, 10, 11, 13, 14, 11, 11, 28, 13, 14, 26, 13, 11, 13, 17, 14, 10, 10]
  ]

  for (let i = 0; i < Sunshine.length; i++) {
    meemmaseedbest.push({
      Sunshine: Sunshine[i],
      Temperature: Temperature[i],
      Wind: Wind[i],
      AdditionalIrrigation: AdditionalIrrigation[i],
      ModelEstimation: ModelEstimation[index - 1][i],
      ModelError: ModelError[index - 1][i],
    })
  }

  return meemmaseedbest.map((val, key) => {
    return {
      key: key,
      Sunshine: val.Sunshine,
      Temperature: val.Temperature,
      Wind: val.Wind,
      AdditionalIrrigation: val.AdditionalIrrigation,
      ModelEstimation: val.ModelEstimation,
      ModelError: val.ModelError,
    }
  })
}