import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts'

import 'antd/dist/antd.css'

import './CropModel.css'

export default function ChartModel({ VussanutModelError, MeemmaseedModelError }) {

  const getData = () => {
    const data = [];
    for (let i = 0; i < VussanutModelError.length; i++) {
      data.push({
        Meemmaseed: MeemmaseedModelError[i].ModelError,
        Vussanut: VussanutModelError[i].ModelError,
        name: i
      })
    }
    return data;
  }

  const navigate = useNavigate();
  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
         // credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <ResponsiveContainer width='100%' height={600}>
      <LineChart width={500} height={300} data={getData()}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          dataKey='name'
          interval={0}
          label={{
            value: 'Index',
            position: 'insideBottom',
            offset: -20,
          }}
        />
        <YAxis
          domain={[0, 40]}
          ticks={[0, 5, 10, 15, 20, 25, 30, 35, 40]}
        />
        <Tooltip />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="Meemmaseed" name="Meemmaseed" stroke="#0D00FF" activeDot={{ r: 8 }} />
        <Line type="monotone" dataKey="Vussanut" name="Vussanut" stroke="#03AA3E" />
      </LineChart>
    </ResponsiveContainer>
  )
}
