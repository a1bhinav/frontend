import { useEffect } from "react";
import { NavLink, useNavigate } from "react-router-dom";

import { Layout, Button } from "antd";
import "antd/dist/antd.css";

const { Content } = Layout;

const TransitionLearning = () => {
  const navigate = useNavigate();

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers:  {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <div className="total-screen">
      <Layout className="layout">
        <h1 className="header-style">Transition Training Round</h1>
        <Content className="site-layout-content">
          <div>
            <p className="card-text">
              You will now start with the training phase. First, you will see statistics from 20 simulated rounds for both crops.
              For each crop, you will see the environmental factors, the optimal additional irrigation amount,
              and the AI System’s irrigation prediction.
              These information can help you learn about the crops, and how to make good irrigation predictions.
              <br />
              <br />
              Additionally, you will see the AI System’s error curve for both crops. The error curve shows for each round, how many points
              the AI System’s prediction was off by. Each point that a prediction is off by reduces your bonus income
              during the Official Rounds.
            </p>
          </div>
          <div className="button-container">
            <NavLink to="/crop-model">
              <Button type="primary" shape="round" size="large">
                Crop and Model Information
              </Button>
            </NavLink>
          </div>
        </Content>
      </Layout>
    </div>
  );
};

export default TransitionLearning;
