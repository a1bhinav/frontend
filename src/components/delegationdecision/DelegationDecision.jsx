import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { Layout, Button } from "antd";
import "antd/dist/antd.css";
import styles from "../instructionquestion/style.module.css";

const { Content } = Layout;

const DelegationDecision = () => {
  const navigate = useNavigate();

  const [q, setQ] = useState({
    question: "What would you prefer?",
    options: ["Your own decision", "Algorithm's decision"],
    answer: -1,
  });

  const updateQuestion = (ans) => {
    q.answer = ans;
    setQ({ ...q });
  };

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
         // credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <div className="total-screen">
      <Layout className="layout">
        <h1 className="header-style">{q.question}</h1>
        <Content className="site-layout-content">
          <div style={{ marginBottom: "5%", textAlign: "center" }}>
            <button
              className={
                q.answer === 0 ? styles.bigblueSelected : styles.bigblue
              }
              type="radio"
              onClick={() => updateQuestion(0)}
            >
              {q.options[0]}
            </button>
            <br />
            <button
              className={
                q.answer === 1 ? styles.bigblueSelected : styles.bigblue
              }
              type="radio"
              onClick={() => updateQuestion(1)}
            >
              {q.options[1]}
            </button>
          </div>
          <div className="button-container">
            <Button
              type="primary"
              shape="round"
              size="large"
              disabled={q.answer === -1}
              onClick={async () => {
                try {
                  const P_ID = await sessionStorage.getItem("P_ID");

                  const res = await fetch("https://human-computer-interaction-backend.onrender.com/api/saveDelegationDecision", {
                    method: "POST",
                    headers: {  
                            Accept: "application/json",  
                            "Access-Control-Allow-Origin": "*",  
                            "Access-Control-Allow-Methods": "*",  
                            "Content-Type": "application/json",  
                          },  
                    body: JSON.stringify({
                      P_ID: P_ID,
                      delegationDecision: q.options[q.answer],
                    }),
                   // credentials: "include",
                  });
                  const data = await res.json();

                  if (!(res.status === 201)) {
                    throw new Error(res.err);
                  } else {
                    console.log(data.message);
                    navigate("/");
                  }
                } catch (err) {
                  console.log(err);
                }
              }}
            >
              Submit
            </Button>
          </div>
        </Content>
      </Layout>
    </div>
  );
};

export default DelegationDecision;
