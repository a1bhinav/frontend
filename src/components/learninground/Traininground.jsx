import React, { useState, useEffect } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import ImageMeemmaseed from '../../resources/Meemmaseed.jpeg'
import ImageVussanut from '../../resources/Vussanut.jpeg'
import { Layout, Col, Row, Button, Modal, Popover, Table } from 'antd'
import 'antd/dist/antd.css'
import {
  meemmaseedbestcolumns,
  getMeemmaseedbest,
  getVussanutbest,
  getMeemmaseedRare,
  getVussanutRare,
  getMeemmaseedBPRare,
  getVussanutBPRare,
  getMeemmaseedCont,
  getVussanutCont,
} from '../cropmodel/CropModel'
import ChartModel from '../cropmodel/ChartModel'

import { getTreatment1, getTreatment2, getTreatment3, getTreatment4, getTreatment5, getTreatment6 } from './TrainingData';

const { Content } = Layout


const Seedx = ({ }) => {
  const [page, setPage] = useState(0)
  const currentPage = [1, 2, 3];
  const getTreatment = () => {
    const treat = sessionStorage.getItem('treatment')
    switch (treat) {
      case 1:
      case "1":
        return getTreatment1();
      case 2:
      case '2':
        return getTreatment2();
      case 3:
      case '3':
        return getTreatment3();
      case 4:
      case '4':
        return getTreatment4();
      case 5:
      case '5':
        return getTreatment5();
      case 6:
      case '6':
        return getTreatment6();
    }
  }
  const [treatment, setTreatment] = useState(getTreatment());

  useEffect(() => {
    if (!treatment) {
      const newTreat = getTreatment();
      setTreatment(newTreat);
    }
  }, [])

  const navigate = useNavigate();

  const randomtype1 = sessionStorage.getItem('randomtype1');
  const randomtype2 = sessionStorage.getItem('randomtype2');
  const [Meemmaseed, setMeemmaseed] = useState();
  const [Vussanut, setVussanut] = useState();

  const getTable = (treatment) => {
    switch (treatment) {
      case 1:
      case '1':
        setMeemmaseed(getMeemmaseedbest());
        setVussanut(getVussanutbest());
        break;
      case 2:
      case "2":
        setMeemmaseed(getMeemmaseedRare(randomtype1));
        setVussanut(getVussanutbest());
        break;
      case 3:
      case '3':
        setMeemmaseed(getMeemmaseedbest());
        setVussanut(getVussanutRare(randomtype2));
        break;
      case 4:
      case '4':
        setMeemmaseed(getMeemmaseedBPRare(randomtype1));
        setVussanut(getVussanutBPRare(randomtype2));
        break;
      case 5:
      case '5':
        setMeemmaseed(getMeemmaseedCont(randomtype1));
        setVussanut(getVussanutBPRare(randomtype2));
        break;
      case 6:
      case '6':
        setMeemmaseed(getMeemmaseedBPRare(randomtype1));
        setVussanut(getVussanutCont(randomtype2));
        break;
    }
  }

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          } else {
            const treatment = data.treatment
            getTable(treatment);
            sessionStorage.setItem('treatment', treatment)
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  const getPageContent = (pageno) => {
    switch (pageno) {
      case 1:
        return 'Click here to review the training information for Meemmaseed'
      case 2:
        return 'Click here to review the training information for Vussanut'
      case 3:
        return "Click here to review the AI System's error curve for both crops"
    }
  }

  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [random, setRandomNumber] = useState();

  useEffect(() => {
    const newrandom = Math.floor(Math.random() * 5);
    if (!random) {
      setRandomNumber(newrandom);
    }
  })

  const [showGraph, setShowGraph] = useState(false)

  const [values1, setValues1] = useState([])

  const [values2, setValues2] = useState([])

  const [data, setData] = useState([])

  const value = undefined;

  const [currentValue1, setCurrentValue1] = useState(value)

  const [currentValue2, setCurrentValue2] = useState(value)

  const [submit, setSubmit] = useState(false)

  const handleModelPage = () => {
    setPage(0)
  }

  const handleOk = () => {

    values1.push(currentValue1)
    setValues1(values1)

    values2.push(currentValue2)
    setValues2(values2)

    data.push({ ...treatment[currentQuestion], currentValue1, currentValue2 })
    setData(data);

    if (currentQuestion === 9) {
      setShowGraph(true)
    } else {
      setCurrentQuestion(currentQuestion + 1)
    }

    setRandomNumber("");
    setSubmit(false)
    setCurrentValue1("")
    setCurrentValue2("")
  }

  return (
    <div className='flex-column-center'>
      <Layout className='layout'>
        {currentQuestion + 1 !== 10 ? (
          <h1 className='header-style-normal' style={{ fontSize: '25px' }}>
            Training Round {currentQuestion + 1}/10 - Irrigation
          </h1>
        ) : (
          <h1 className='header-style-normal' style={{ fontSize: '25px' }}>
            Training Round 10/10 - Irrigation
          </h1>
        )}

        <Content>
          {showGraph ? (
            <div className='total-screen'>
              <Layout className='layout'>
                <Content className='site-layout-content'>
                  <div>
                    <p className='card-text'>
                      You have successfully completed the Training Phase.
                      You will now continue to the Official Rounds. The
                      Official Rounds determine your bonus income. You will
                      make 10 estimations for both crops. For each
                      estimation, you can choose to rely on yourself or the
                      model’s estimation. Each point your implemented
                      estimation is off by reduces your bonus income for
                      that round by 1 Coin.
                    </p>
                    <h2 style={{ textAlign: 'justify' }}>
                      Your final bonus payoff will be determined by your performance in one randomly chosen Official Round.
                      Thus, each Official Round could be the one determining your bonus income from this task.
                    </h2>
                  </div>
                </Content>
              </Layout>

              <div style={{ marginTop: '4vh' }}>
                <Row
                  className='flex justify-center'
                  gutter={{
                    xs: 8,
                    sm: 16,
                    md: 24,
                    lg: 32,
                  }}
                >
                  <Col className='gutter-row' span={12}>
                    <div className='button-container'>
                      <NavLink to='/officialround'>
                        <Button
                          type='primary'
                          shape='round'
                          size='large'
                          style={{ width: '275px' }}
                          onClick={async () => {
                            try {
                              const P_ID = await sessionStorage.getItem("P_ID");

                              const res = await fetch("https://human-computer-interaction-backend.onrender.com/api/trainingData", {
                                method: "POST",
                                headers: {  
                            Accept: "application/json",  
                            "Access-Control-Allow-Origin": "*",  
                            "Access-Control-Allow-Methods": "*",  
                            "Content-Type": "application/json",  
                          },  
                                body: JSON.stringify({
                                  P_ID: P_ID, TrainingData: data
                                }),
                               // credentials: "include",
                              });
                              if (!(res.status === 200)) {
                                alert("Some Error Occured");
                              } else {
                                navigate("/officialround");
                              }
                            } catch (err) {
                              console.log(err);
                            }
                          }}
                        >
                          Go to Official Rounds
                        </Button>
                      </NavLink>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          ) : (
            <div className='flex-column-center'>
              <h2
                className='text-center'
                style={{
                  margin: '0 20% 1rem',
                  fontWeight: '400',
                  fontSize: '1rem',
                }}
              >
                Hint: Meemmaseed is known to be unaffected by different
                temperatures, but very sensitive to wind speed. Therefore, to
                estimate the additional irrigation needed for your Meemmaseed
                crop, you should focus mainly on the third input variable Wind
                and ignore the second environmental variable Temperature.
              </h2>
              <h2
                className='text-center'
                style={{
                  margin: '0 20% 1rem',
                  fontWeight: '400',
                  fontSize: '1rem',
                }}
              >
                For Vussanut, there are no additional information available.
              </h2>
              <table style={{ margin: '32px 0' }}>
                <tr>
                  <td>Sunshine ( 1 - 18 )</td>
                  <td>Temperature ( 32 - 108 Fahrenheit )</td>
                  <td>Wind ( 5 - 61 km/hr )</td>
                </tr>
                <tr>
                  <td>
                    <b>{treatment[currentQuestion].Sunshine}</b>
                  </td>
                  <td>
                    <b>{treatment[currentQuestion].Temperature}</b>
                  </td>
                  <td>
                    <b>{treatment[currentQuestion].Wind}</b>
                  </td>
                </tr>
              </table>
              <h2 className='text-center'>
                How much irrigation do your crops need?
              </h2>
              <h2 className='text-center'>
                [Please enter a value between 0 and 70]
              </h2>
              <div
                className='input-area flex-column-center'
                style={{ marginBottom: '40px', marginTop: '40px' }}
              >
                <div
                  className='input-plus-submit flex-row-center'
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '24px',
                  }}
                >
                  <h1
                    className='header-style-normal'
                    style={{
                      fontSize: '25px',
                      width: '200px',
                      textAlign: 'right',
                    }}
                  >
                    Meemmaseed
                  </h1>
                  {!showGraph && (
                    <img
                      src={ImageMeemmaseed}
                      alt='Meemmaseed'
                      className='logo'
                    />
                  )}

                  <input
                    type='number'
                    required
                    min={0}
                    max={70}
                    id={`q${currentQuestion + 1}`}
                    name={`q${currentQuestion + 1}`}
                    value={currentValue1}
                    onChange={(e) => {
                      let value = e.target.value;
                      if (value < 0) {
                        value = 0;
                        setCurrentValue1(0);
                      } else if (value > 70) {
                        value = 70;
                        setCurrentValue1(70)
                      } else {
                        setCurrentValue1(value)
                      }
                    }}
                    style={{ width: '150px' }}
                    disabled={submit}
                  />
                </div>
                <div
                  className='input-plus-submit flex-row-center'
                  style={{
                    marginBottom: '32px',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '24px',
                  }}
                >
                  <h1
                    className='header-style-normal'
                    style={{
                      fontSize: '25px',
                      width: '200px',
                      textAlign: 'right',
                    }}
                  >
                    Vussanut
                  </h1>
                  {!showGraph && (
                    <img
                      src={ImageVussanut}
                      alt='Vussanut'
                      className='logo'
                    />
                  )}

                  <input
                    type='number'
                    inputMode='numeric'
                    required
                    min={0}
                    max={70}
                    id={`q${currentQuestion + 1}`}
                    name={`q${currentQuestion + 1}`}
                    value={currentValue2}
                    onChange={(e) => {
                      let value = e.target.value;
                      if (value < 0) {
                        value = 0
                        setCurrentValue2(0);
                      } else if (value > 70) {
                        value = 70
                        setCurrentValue2(70);
                      } else {
                        setCurrentValue2(value)
                      }
                    }}
                    style={{ width: '150px' }}
                    disabled={submit}
                  />
                </div>

                <input
                  style={
                    currentValue1 && currentValue2
                      ? {
                        background: '#FE188B',
                        width: '150px',
                        cursor: 'pointer',
                      }
                      : {
                        background: '#FDC6E2',
                        width: '150px',
                      }
                  }
                  type='submit'
                  defaultValue='Submit'
                  disabled={!currentValue1 && !currentValue2}
                  onClick={() => {
                    if (
                      currentValue1 >= 0 &&
                      currentValue1 <= 70 &&
                      currentValue1 !== '' &&
                      currentValue2 >= 0 &&
                      currentValue2 <= 70 &&
                      currentValue2 !== ''
                    ) {
                      setSubmit(true)
                      return
                    }
                    alert('Please provide an input between 0 to 70')
                  }}
                />
              </div>
            </div>
          )}

          {submit && (
            <Modal
              title='Feedback'
              open={submit}
              onOk={handleOk}
              okText='Next'
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleOk}
                  >
                    Next
                  </Button>
                </div>,
              ]}
            >
              <div
                className='flex justify-center align-center'
                style={{ flexDirection: 'column' }}
              >
                <h1
                  className='text-center'
                  style={{
                    width: '283',
                    fontSize: '23px',
                  }}
                >
                  Environmental Factors
                </h1>
                <table style={{ margin: '32px 0' }}>
                  <tr>
                    <td>Sunshine ( 1 - 18 )</td>
                    <td>Temperature ( 32 - 108 Fahrenheit )</td>
                    <td>Wind ( 5 - 61 km/hr )</td>
                  </tr>
                  <tr>
                    <td>
                      {treatment[currentQuestion].Sunshine}
                    </td>
                    <td>
                      {treatment[currentQuestion].Temperature}
                    </td>
                    <td>
                      {treatment[currentQuestion].Wind}
                    </td>
                  </tr>
                </table>
              </div>
              <div className='flex justify-center align-center'>
                <h1
                  className='text-center'
                  style={{ width: '250px', fontSize: '23px' }}
                >
                  For Meemaseed
                </h1>
                <img src={ImageMeemmaseed} alt='Meemmaseed' />
              </div>
              <table style={{ width: '100%' }}>
                <tr>
                  <td>Your irrigation</td>
                  <td>The AI System's irrigation prediction</td>
                  <td>The optimal amount of irrigation</td>
                </tr>
                <tr>
                  <td>
                    <b>{currentValue1}</b>
                  </td>
                  <td>
                    <b>{treatment[currentQuestion].Meemmaseed_System_Error}</b>
                  </td>
                  <td>
                    <b>{treatment[currentQuestion].Meemmaseed_Optimal_Irrigation}</b>
                  </td>
                </tr>
              </table>
              <div className='flex justify-center align-center'>
                <h1
                  className='text-center'
                  style={{ width: '250px', fontSize: '23px' }}
                >
                  For Vussanut
                </h1>
                <img src={ImageVussanut} alt='Vussanut' />
              </div>
              <table style={{ width: '100%' }}>
                <tr>
                  <td>Your irrigation</td>
                  <td>The AI System's irrigation prediction</td>
                  <td>The optimal amount of irrigation</td>
                </tr>
                <tr>
                  <td>
                    <b>{currentValue2}</b>
                  </td>
                  <td>
                    <b>{treatment[currentQuestion].Vussanut_System_Error}</b>
                  </td>
                  <td>
                    <b>{treatment[currentQuestion].Vussanut_Optimal_Irrigation}</b>
                  </td>
                </tr>
              </table>
            </Modal>
          )}


          {page === 1 && !showGraph && (
            <Modal
              style={{ width: '100%' }}
              // title='Meemmaseed Best Possible Algorithm'
              open={page === 1}
              onOk={handleModelPage}
              okText='Next'
              onCancel={() => {
                setPage(0)
              }}
              width={800}
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleModelPage}
                  >
                    Close
                  </Button>
                </div>,
              ]}
            >
              <Table
                style={{ marginTop: 0 }}
                dataSource={Meemmaseed}
                columns={meemmaseedbestcolumns}
                pagination={false}
              />
            </Modal>
          )}

          {page === 2 && !showGraph && (
            <Modal
              style={{ width: '100%' }}
              // title='Vussanut Rare Algorithm'
              open={page === 2}
              onOk={handleModelPage}
              okText='Next'
              onCancel={() => {
                setPage(0)
              }}
              width={800}
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleModelPage}
                  >
                    Close
                  </Button>
                </div>,
              ]}
            >
              <Table
                style={{ marginTop: 0 }}
                dataSource={Vussanut}
                columns={meemmaseedbestcolumns}
                pagination={false}
              />
            </Modal>
          )}

          {page === 3 && !showGraph && (
            <Modal
              style={{ width: '100%' }}
              // title='Simple Line Graph Meemmaseedbest v/s Vussanutseedrare'
              open={page === 3}
              onOk={handleModelPage}
              okText='Next'
              onCancel={() => {
                setPage(0)
              }}
              width={800}
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleModelPage}
                  >
                    Close
                  </Button>
                </div>,
              ]}
            >
              <p className='card-text flex-column-center'>
                <ChartModel VussanutModelError={Vussanut} MeemmaseedModelError={Meemmaseed} />
              </p>
            </Modal>
          )}

          {!showGraph && (
            <div id='wrapper'>
              <div
                class='b-pagination-outer column'
                style={{ maxWidth: '500px' }}
              >
                <ul id='border-pagination'>
                  {currentPage.map((currentPage) => (
                    <li key={currentPage}>
                      {currentPage === page && (
                        <button
                          style={{ width: '100%' }}
                          className='active'
                          onClick={() => {
                            setPage(currentPage)
                          }}
                        >
                          {getPageContent(currentPage)}
                        </button>
                      )}
                      {currentPage !== page && (
                        <button
                          style={{ width: '100%' }}
                          onClick={() => {
                            setPage(currentPage)
                          }}
                        >
                          {getPageContent(currentPage)}
                        </button>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          )}
        </Content>
      </Layout>
    </div>
  )
}

export default Seedx
