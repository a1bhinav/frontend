export function generateNums1() {
  return [
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 77) + 32,
      z: Math.floor(Math.random() * 57) + 5,
    },
  ];
}

// Function to generate random number between the range of min and max
function randomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

// Function to return +3 if random number is negative and -3 if random number is positive
function randomerrorincreaser(randomNumber) {
  if (randomNumber < 0) {
    return 3;
  } else {
    return -3;
  }
}

// Optimal Irrigation (Meemmaseed)
export function generateCorrectData1(nums) {
  return [
    Math.floor( 0.1 * nums[0].x + 0.9 * nums[0].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[1].x + 0.9 * nums[1].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[2].x + 0.9 * nums[2].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[3].x + 0.9 * nums[3].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[4].x + 0.9 * nums[4].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[5].x + 0.9 * nums[5].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[6].x + 0.9 * nums[6].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[7].x + 0.9 * nums[7].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[8].x + 0.9 * nums[8].z - randomNumber(-5, 5)),
    Math.floor( 0.1 * nums[9].x + 0.9 * nums[9].z - randomNumber(-5, 5)),
  ];
}

// Memmaseed Continous Error (x = sunshine, y = Temperature, z = Wind)
export function generateModelData1(nums) {
  return [
    Math.floor(
      
      0.1 * nums[0].x +
      0.9 * nums[0].z 
    ),
    Math.floor(
      
      0.1 * nums[1].x +
      0.9 * nums[1].z 
    ),
    Math.floor(
     
      0.1 * nums[2].x +
      0.9 * nums[2].z 
    ),
    Math.floor(
      
      0.1 * nums[3].x +
      0.9 * nums[3].z 
    ),
    Math.floor(
    
      0.1 * nums[4].x +
      0.9 * nums[4].z 
    ),
    Math.floor(
      
      0.1 * nums[5].x +
      0.9 * nums[5].z 
    ),
    Math.floor(
      
      0.1 * nums[6].x +
      0.9 * nums[6].z 
    ),
    Math.floor(
      
      0.1 * nums[7].x +
      0.9 * nums[7].z 
    ),
    Math.floor(
      
      0.1 * nums[8].x +
      0.9 * nums[8].z 
    ),
    Math.floor(
     
      0.1 * nums[9].x +
      0.9 * nums[9].z 
    ),
  ];
}

// function to return random error with a probability 0.9 with a range of -4 to 4 and 0.1 with a range of -30 or 30
export function randProb() {
  var rnd = Math.random();
  if (rnd < 0.9) return randomNumber(-5, 5);
  else if (rnd < 0.95) return 30;
  else return -30;
}

// Memmaseed Rare Error
export function generateModelDataRare(nums) {
  return [
    Math.floor(40 + 0.1 * nums[0].x + 0.9 * nums[0].z + randProb()),
    Math.floor(40 + 0.1 * nums[1].x + 0.9 * nums[1].z + randProb()),
    Math.floor(40 + 0.1 * nums[2].x + 0.9 * nums[2].z + randProb()),
    Math.floor(40 + 0.1 * nums[3].x + 0.9 * nums[3].z + randProb()),
    Math.floor(40 + 0.1 * nums[4].x + 0.9 * nums[4].z + randProb()),
    Math.floor(40 + 0.1 * nums[5].x + 0.9 * nums[5].z + randProb()),
    Math.floor(40 + 0.1 * nums[6].x + 0.9 * nums[6].z + randProb()),
    Math.floor(40 + 0.1 * nums[7].x + 0.9 * nums[7].z + randProb()),
    Math.floor(40 + 0.1 * nums[8].x + 0.9 * nums[8].z + randProb()),
    Math.floor(40 + 0.1 * nums[9].x + 0.9 * nums[9].z + randProb()),
  ];
}
