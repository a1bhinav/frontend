import React, { useState, useEffect } from 'react'
import {
  NavLink,
  useNavigate,
  BrowserRouter as Router,
  Route,
  Redirect,
} from 'react-router-dom'

import {
  generateNums1,
  //   generateModelData1,
  //   generateCorrectData1,
} from './seed1/utils'
// import {
//   generateModelData2,
//   generateCorrectData2,
// } from './seed4/utils'
import {
  meemmaseedbestcolumns,
  getMeemmaseedbest,
  getVussanutbest,
  getMeemmaseedRare,
  getVussanutRare,
  getMeemmaseedBPRare,
  getVussanutBPRare,
  getMeemmaseedCont,
  getVussanutCont,
} from '../cropmodel/CropModel'
import ChartModel from '../cropmodel/ChartModel'
import ImageMeemmaseed from '../../resources/Meemmaseed.jpeg'
import ImageVussanut from '../../resources/Vussanut.jpeg'
import { Layout, Button, Modal, Table, Radio, Space } from 'antd'
import 'antd/dist/antd.css'
import { treatment1, treatment2, treatment3, treatment4, treatment5, treatment6 } from './formulae'

const { Content } = Layout

const OfficialRound = ({
}) => {
  const [page, setPage] = useState(0)
  const currentPage = [1, 2, 3]
  const navigate = useNavigate();

  const getPageContent = (pageno) => {
    switch (pageno) {
      case 1:
        return 'Click here to review the training information for Meemmaseed'
      case 2:
        return 'Click here to review the training information for Vussanut'
      case 3:
        return "Click here to review the AI System's error curve for both crops"
    }
  }

  const [currentQuestion, setCurrentQuestion] = useState(0)
  const [nums1, setNums1] = useState(generateNums1());

  const getTreatment = () => {
    const treat = sessionStorage.getItem('treatment')
    switch (treat) {
      case 1:
      case "1":
        return treatment1(nums1);
      case 2:
      case '2':
        return treatment2(nums1);
      case 3:
      case '3':
        return treatment3(nums1);
      case 4:
      case '4':
        return treatment4(nums1);
      case 5:
      case '5':
        return treatment5(nums1);
      case 6:
      case '6':
        return treatment6(nums1);
    }
  }

  const [treatment, setTreatment] = useState(getTreatment());

  useEffect(() => {
    if (!treatment) {
      const newTreat = getTreatment();
      setTreatment(newTreat);
    }
    if (!nums1) {
      const num = generateNums1();
      setNums1(num);
    }
  }, [])

  const [onChangeInput1, setOnChangeInput1] = useState(false)
  const [onChangeInput2, setOnChangeInput2] = useState(false)

  const [values1, setValues1] = useState([])
  const [values2, setValues2] = useState([])

  const [choices1, setChoices1] = useState([])
  const [choices2, setChoices2] = useState([])

  const [currentValue1, setCurrentValue1] = useState()
  const [currentValue2, setCurrentValue2] = useState()

  const [submit, setSubmit] = useState(false)
  const [selectedOption1, setSelectedOption1] = useState(false)
  const [selectedOption2, setSelectedOption2] = useState(false)
  const [isNextButtonDisabled, setIsNextButtonDisabled] = useState(true)
  const handleModelPage = () => {
    setPage(0)
  }

  const handleOptionChange1 = (e) => {
    setSelectedOption1(e.target.value)
    if (e.target.value != false && selectedOption2 != false) {
      setIsNextButtonDisabled(false)
    }
    // if (e.target.value === 'model') {
    //   const modelPrediction = modelValues1[0]
    //   setCurrentValue1(modelPrediction)
    //   setOnChangeInput1(true)
    // } else {
    //   setCurrentValue1(0)
    // }
  }

  const handleOptionChange2 = (e) => {
    setSelectedOption2(e.target.value)
    if (selectedOption1 != false && e.target.value != false) {
      setIsNextButtonDisabled(false)
    }
    // if (e.target.value === 'model') {
    //   const modelPrediction = modelValues2[0]
    //   setCurrentValue2(modelPrediction)
    //   setOnChangeInput2(true)
    // } else {
    //   setCurrentValue2(0)
    // }
  }

  const submitOfficialRound = async () => {
    try {
      const P_ID = await sessionStorage.getItem("P_ID");

      const data = [];
      for (let i = 0; i < values1.length; i++) {
        data.push({
          value1: values1[i],
          value2: values2[i],
          sunshine: nums1[i].x,
          Temperature: nums1[i].y,
          Wind: nums1[i].z,
          MeemmaseedChoice: choices1[i],
          VussanutChoice: choices2[i],
          MeemmaseedOptimal: treatment[i].MeemmaseedOptimal,
          MeemmaseedOptimalRandom: treatment[i].MeemmaseedOptimalRandom,
          MeemmassedModelValue: treatment[i].MeemmaseedModalError,
          MeemmaseedModalValueRandom: treatment[i].MeemmaseedModalErrorRandom,
          VussanutOptimal: treatment[i].VussanutOptimal,
          VussanutOptimalRandom: treatment[i].VussanutOptimalRandom,
          VussanutModalValue: treatment[i].VussanutModalError,
          VussanutModalValueRadnom: treatment[i].VussanutModalErrorRandom,
          // Bonus_Meemmaseed = 35 - | MeemmaseedOptimal - (if (choice = "Your deci") {value1} else {MeemmassedModelValue}) |
        })
      }

      let bonus_Meemmaseed = 0;
      let bonus_Vussanut = 0;

      for (let i = 0; i < values1.length; i++) {
        if (choices1[i] === 'your') {
          bonus_Meemmaseed += 35 - Math.abs(treatment[i].MeemmaseedOptimal - values1[i])
        } else {
          bonus_Meemmaseed += 35 - Math.abs(treatment[i].MeemmaseedOptimal - treatment[i].MeemmaseedModalError)
        }

        if (choices2[i] === 'your') {
          bonus_Vussanut += 35 - Math.abs(treatment[i].VussanutOptimal - values2[i])
        } else {
          bonus_Vussanut += 35 - Math.abs(treatment[i].VussanutOptimal - treatment[i].VussanutModalError)
        }
      }

      const bonus = {
        bonus_Meemmaseed: bonus_Meemmaseed,
        bonus_Vussanut: bonus_Vussanut
      }

      const res = await fetch("https://human-computer-interaction-backend.onrender.com/api/officialData", {
        method: "POST",
        headers: {  
          Accept: "application/json",  
          "Access-Control-Allow-Origin": "*",  
          "Access-Control-Allow-Methods": "*",  
          "Content-Type": "application/json",  
        },  
        body: JSON.stringify({
          P_ID: P_ID, OfficialData: data, Bonus: bonus
        }),
       // credentials: "include",
      });
      if (!(res.status === 200)) {
        alert("Some Error Occured");
      } else {
        navigate("/questionnaire");
      }
    } catch (err) {
      console.log(err);
    }
  }

  const handleOk = () => {
    setSelectedOption1(false)
    setSelectedOption2(false)

    // if (currentQuestion < 9) {
    //   // nums1.push({
    //   //   x: Math.floor(Math.random() * 18) + 1,
    //   //   y: Math.floor(Math.random() * 73) + 32,
    //   //   z: Math.floor(Math.random() * 61) + 1,
    //   // })
    //   // setNums1(nums1)

    //   // modelValues1.push(
    //   //   Math.floor(
    //   //     0.3 * nums1[currentQuestion].x +
    //   //     0.3 * nums1[currentQuestion].y +
    //   //     0.3 * nums1[currentQuestion].z -
    //   //     Math.random() * 10 -
    //   //     7
    //   //   )
    //   // )
    //   // setModelValues1(modelValues1)

    //   // correctValues1.push(
    //   //   Math.floor(
    //   //     0.3 * nums1[0].x +
    //   //     0.3 * nums1[0].y +
    //   //     0.3 * nums1[0].z +
    //   //     Math.random() * 10 -
    //   //     5
    //   //   )
    //   // )
    //   // setCorrectValues1(correctValues1)

    //   // modelValues2.push(
    //   //   Math.floor(
    //   //     0.2 * nums1[currentQuestion].x +
    //   //     0.3 * nums1[currentQuestion].y -
    //   //     0.5 * nums1[currentQuestion].z
    //   //   )
    //   // )
    //   // setModelValues2(modelValues2)

    //   // correctValues2.push(
    //   //   Math.floor(
    //   //     0.2 * nums1[currentQuestion].x +
    //   //     0.3 * nums1[currentQuestion].y -
    //   //     0.5 * nums1[currentQuestion].z
    //   //   )
    //   // )
    //   // setCorrectValues2(correctValues2)
    // }

    values1.push(currentValue1)
    setValues1(values1)

    values2.push(currentValue2)
    setValues2(values2)

    choices1.push(selectedOption1)
    setChoices1(choices1)

    choices2.push(selectedOption2)
    setChoices2(choices2)

    if (currentQuestion === 9) {
      submitOfficialRound();
      // <NavLink to="/questionnaire"></NavLink>
    } else {
      setCurrentQuestion(currentQuestion + 1)
      setIsNextButtonDisabled(true)
    }

    setOnChangeInput1(false)
    setOnChangeInput2(false)
    setSubmit(false)
    // setSubmit1(false)
    // setSubmit2(false)
    setSelectedOption1(false)
    setSelectedOption2(false)
    // setIsModalOpen(false)
    setCurrentValue1('')
    setCurrentValue2('')
  }

  const randomtype1 = sessionStorage.getItem('randomtype1');
  const randomtype2 = sessionStorage.getItem('randomtype2');
  const [Meemmaseed, setMeemmaseed] = useState();
  const [Vussanut, setVussanut] = useState();

  const getTable = (treatment) => {
    switch (treatment) {
      case 1:
      case '1':
        setMeemmaseed(getMeemmaseedbest());
        setVussanut(getVussanutbest());
        break;
      case 2:
      case "2":
        setMeemmaseed(getMeemmaseedRare(randomtype1));
        setVussanut(getVussanutbest());
        break;
      case 3:
      case '3':
        setMeemmaseed(getMeemmaseedbest());
        setVussanut(getVussanutRare(randomtype2));
        break;
      case 4:
      case '4':
        setMeemmaseed(getMeemmaseedBPRare(randomtype1));
        setVussanut(getVussanutBPRare(randomtype2));
        break;
      case 5:
      case '5':
        setMeemmaseed(getMeemmaseedCont(randomtype1));
        setVussanut(getVussanutBPRare(randomtype2));
        break;
      case 6:
      case '6':
        setMeemmaseed(getMeemmaseedBPRare(randomtype1));
        setVussanut(getVussanutCont(randomtype2));
        break;
    }
  }

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
         // credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          } else {
            const treatment = data.treatment
            getTable(treatment);
            sessionStorage.setItem('treatment', treatment)
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  // const getErrorData = async () => {
  //   let error0 = 0,
  //     error1 = 0
  //   const errors = []

  //   for (let i = 0; i < correctValues1.length; i++) {
  //     error0 += Math.abs(correctValues1[i] - modelValues1[i])
  //     error1 += Math.abs(correctValues1[i] - values1[i])
  //   }

  //   errors.push(error0)
  //   errors.push(error1)

  //   setErrorData(errors)
  // }

  // const popContent1 = (index) => (
  //   <div>
  //     <table id='result-table'>
  //       <tr>
  //         <th>Sunshine ( 1 - 18 )</th>
  //         <th>Temperature ( 32 - 104 Fahrenheit )</th>
  //         <th>Wind ( 1 - 61 km/hr )</th>
  //       </tr>
  //       <tr>
  //         <td>
  //           <b>{nums1[index].x}</b>
  //         </td>
  //         <td>
  //           <b>{nums1[index].y}</b>
  //         </td>
  //         <td>
  //           <b>{nums1[index].z}</b>
  //         </td>
  //       </tr>
  //     </table>
  //   </div>
  // )

  // useEffect(() => {
  //   if (showGraph) {
  //     getErrorData().then(() => {
  //       console.log('getter fn called')
  //       setLoading(false)
  //     })
  //   }
  // }, [showGraph])

  // useEffect(() => {
  //   // Update the submit state whenever both submit1 and submit2 are true
  //   setSubmit(submit1 && submit2)
  // }, [submit1, submit2])

  // const radioStyle = {
  //   borderRadius: '100%',
  //   // backgroundColor: '#000',
  //   border: '2px solid #000',
  // }
  return (
    <div className='flex-column-center'>
      <Layout className='layout'>
        <h1 className='header-style-normal' style={{ fontSize: '25px' }}>
          Official Round {currentQuestion + 1}/10 - Irrigation
        </h1>
        <Content>
          <div>
            <div className='flex-column-center'>
              <h2
                className='text-center'
                style={{
                  margin: '0 20% 1rem',
                  fontWeight: '400',
                  fontSize: '1rem',
                }}
              >
                Hint: Meemmaseed is known to be unaffected by different
                temperatures, but very sensitive to wind speed. Therefore, to
                estimate the additional irrigation needed for your Meemmaseed
                crop, you should focus mainly on the third input variable Wind
                and ignore the second environmental variable Temperature.
              </h2>
              <h2
                className='text-center'
                style={{
                  margin: '0 20% 1rem',
                  fontWeight: '400',
                  fontSize: '1rem',
                }}
              >
                For Vussanut, there are no additional information available.
              </h2>
              <table style={{ margin: '32px 0' }}>
                <tr>
                  <td>Sunshine ( 1 - 18 )</td>
                  <td>Temperature ( 32 - 108 Fahrenheit )</td>
                  <td>Wind ( 5 - 61 km/hr )</td>
                </tr>
                <tr>
                  <td>
                    <b>{nums1[currentQuestion].x}</b>
                  </td>
                  <td>
                    <b>{nums1[currentQuestion].y}</b>
                  </td>
                  <td>
                    <b>{nums1[currentQuestion].z}</b>
                  </td>
                </tr>
              </table>
              <h2 className='text-center'>
                How much irrigation do your crops need?
              </h2>
              <h2 className='text-center'>[Please enter a value between 0 and 70]</h2>
              <div
                className='input-area flex-column-center'
                style={{ marginBottom: '40px', marginTop: '40px' }}
              >
                <div
                  className='input-plus-submit flex-row-center'
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '24px',
                  }}
                >
                  <h1
                    className='header-style-normal'
                    style={{
                      fontSize: '25px',
                      width: '200px',
                      textAlign: 'right',
                    }}
                  >
                    Meemmaseed
                  </h1>
                  <img
                    src={ImageMeemmaseed}
                    alt='Meemmaseed'
                    className='logo'
                  />
                  <input
                    type='number'
                    required
                    min={0}
                    max={70}
                    id={`q${currentQuestion + 1}`}
                    name={`q${currentQuestion + 1}`}
                    defaultValue={currentValue1}
                    value={currentValue1}
                    onChange={(e) => {
                      let value = e.target.value;
                      if (value < 0) {
                        value = 0
                        setCurrentValue1(0)
                      } else if (value > 70) {
                        value = 70
                        setCurrentValue1(70)
                      } else {
                        setCurrentValue1(value)
                      }
                      if (
                        value !== '' &&
                        value >= 0 &&
                        value <= 70
                      ) {
                        setOnChangeInput1(true)
                      }
                    }}
                    style={{ width: '150px' }}
                    disabled={submit}
                  />
                </div>
                <div
                  className='input-plus-submit flex-row-center'
                  style={{
                    marginBottom: '32px',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '24px',
                  }}
                >
                  <h1
                    className='header-style-normal'
                    style={{
                      fontSize: '25px',
                      width: '200px',
                      textAlign: 'right',
                    }}
                  >
                    Vussanut
                  </h1>
                  {/* {!showGraph && ( */}
                  <img src={ImageVussanut} alt='Vussanut' className='logo' />
                  {/* )} */}
                  <input
                    type='number'
                    required
                    min={0}
                    max={70}
                    id={`q${currentQuestion + 1}`}
                    name={`q${currentQuestion + 1}`}
                    defaultValue={currentValue2}
                    value={currentValue2}
                    onChange={(e) => {
                      let value = e.target.value;
                      if (value < 0) {
                        value = 0;
                        setCurrentValue2(0);
                      } else if (value > 70) {
                        value = 70;
                        setCurrentValue2(70);
                      } else {
                        setCurrentValue2(value)
                      }
                      if (
                        value !== '' &&
                        value >= 0 &&
                        value <= 70
                      ) {
                        setOnChangeInput2(true)
                      }
                    }}
                    style={{ width: '150px' }}
                    disabled={submit}
                  />
                </div>

                <input
                  style={
                    onChangeInput2 && onChangeInput1
                      ? {
                        background: '#FE188B',
                        cursor: 'pointer',
                        width: '100px',
                      }
                      : {
                        background: '#FDC6E2',
                        width: '100px',
                      }
                  }
                  type='submit'
                  defaultValue='Submit'
                  disabled={!onChangeInput1 && !onChangeInput2}
                  onClick={() => {
                    if (
                      currentValue1 >= 0 &&
                      currentValue1 <= 70 &&
                      currentValue1 !== '' &&
                      currentValue2 >= 0 &&
                      currentValue2 <= 70 &&
                      currentValue2 !== ''
                    ) {
                      setSubmit(true)
                      return
                    }
                    alert('Please provide an input between 0 to 70')
                  }}
                />
              </div>
            </div>
          </div>
          {submit && (
            <div>
              <Modal
                title='Please choose whose irrigation predictions to implement'
                open={submit}
                onOk={handleOk}
                okText='Next'
                footer=
                {
                  // currentQuestion === 9 ?
                  //   <div className='button-container'>
                  //     <NavLink to="/questionnaire">
                  //       <Button
                  //         key='Next'
                  //         type='primary'
                  //         shape='round'
                  //         size='large'
                  //         onClick={handleOk}
                  //         disabled={isNextButtonDisabled}
                  //       >
                  //         Next
                  //       </Button>
                  //     </NavLink>
                  //   </div> :
                  <div className='button-container'>
                    <Button
                      key='Next'
                      type='primary'
                      shape='round'
                      size='large'
                      onClick={handleOk}
                      disabled={isNextButtonDisabled}
                    >
                      Next
                    </Button>
                  </div>
                }
              >
                <div style={{ marginLeft: '5%' }}>
                  <div className='flex'>
                    <h1 style={{ width: '250px' }}>For Meemaseed</h1>
                    <img
                      src={ImageMeemmaseed}
                      alt='Meemmaseed'
                      style={{ marginTop: '3%', marginLeft: '-9%' }}
                    />
                  </div>

                  <Radio.Group
                    onChange={handleOptionChange1}
                    value={selectedOption1}
                    style={{
                      marginBottom: '5%',
                      marginLeft: '1%',
                    }}
                    size='large'
                  >
                    <Space direction='vertical'>
                      <Radio
                        value='your'
                        style={{ fontSize: '1.08rem' }}
                        required
                      >
                        Choose Your Prediction
                      </Radio>
                      <Radio
                        value='model'
                        style={{ fontSize: '1.08rem' }}
                        required
                      >
                        Choose the AI System's Prediction
                      </Radio>
                    </Space>
                  </Radio.Group>
                </div>

                <div style={{ marginLeft: '5%' }}>
                  <div className='flex'>
                    <h1 style={{ width: '250px' }}>For Vussanut{''}</h1>
                    <img
                      src={ImageVussanut}
                      alt='Vussanut'
                      style={{ marginTop: '3%', marginLeft: '-17%' }}
                    />
                  </div>

                  <Radio.Group
                    onChange={handleOptionChange2}
                    value={selectedOption2}
                  >
                    <Space direction='vertical'>
                      <Radio value='your' style={{ fontSize: '1.08rem' }}>
                        Choose Your Prediction
                      </Radio>
                      <Radio value='model' style={{ fontSize: '1.08rem' }}>
                        Choose the AI System's Prediction
                      </Radio>
                    </Space>
                  </Radio.Group>
                </div>
              </Modal>
            </div>
          )}

          {page === 1 && (
            <Modal
              style={{ width: '100%' }}
              // title='Meemmaseed Best Possible Algorithm'
              open={page === 1}
              onOk={handleModelPage}
              okText='Next'
              onCancel={() => {
                setPage(0)
              }}
              width={800}
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleModelPage}
                  >
                    Close
                  </Button>
                </div>,
              ]}
            >
              <Table
                style={{ marginTop: 0 }}
                dataSource={Meemmaseed}
                columns={meemmaseedbestcolumns}
                pagination={false}
              />
            </Modal>
          )}
          {page === 2 && (
            <Modal
              style={{ width: '100%' }}
              // title='Vussanut Rare Algorithm'
              open={page === 2}
              onOk={handleModelPage}
              okText='Next'
              onCancel={() => {
                setPage(0)
              }}
              width={800}
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleModelPage}
                  >
                    Close
                  </Button>
                </div>,
              ]}
            >
              <Table
                style={{ marginTop: 0 }}
                dataSource={Vussanut}
                columns={meemmaseedbestcolumns}
                pagination={false}
              />
            </Modal>
          )}
          {page === 3 && (
            <Modal
              style={{ width: '100%' }}
              // title='Simple Line Graph Meemmaseedbest v/s Vussanutseedrare'
              open={page === 3}
              onOk={handleModelPage}
              okText='Next'
              onCancel={() => {
                setPage(0)
              }}
              width={800}
              footer={[
                <div className='button-container'>
                  <Button
                    key='Next'
                    type='primary'
                    shape='round'
                    size='large'
                    onClick={handleModelPage}
                  >
                    Close
                  </Button>
                </div>,
              ]}
            >
              <p className='card-text flex-column-center'>
                <ChartModel VussanutModelError={Vussanut} MeemmaseedModelError={Meemmaseed} />
              </p>
            </Modal>
          )}
          <div id='wrapper'>
            <div
              class='b-pagination-outer column'
              style={{ maxWidth: '500px' }}
            >
              <ul id='border-pagination'>
                {currentPage.map((currentPage) => (
                  <li key={currentPage}>
                    {currentPage === page && (
                      <button
                        style={{ width: '100%' }}
                        className='active'
                        onClick={() => {
                          setPage(currentPage)
                        }}
                      >
                        {getPageContent(currentPage)}
                      </button>
                    )}
                    {currentPage !== page && (
                      <button
                        style={{ width: '100%' }}
                        onClick={() => {
                          setPage(currentPage)
                        }}
                      >
                        {getPageContent(currentPage)}
                      </button>
                    )}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </Content>
      </Layout>
    </div>
  )
}

export default OfficialRound
