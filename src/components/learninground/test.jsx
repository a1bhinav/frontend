import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const PredictionInput = () => {
  const [selectedOption, setSelectedOption] = useState("");
  const [inputValue, setInputValue] = useState("");
  const navigate = useNavigate();

  const handleOptionChange = (e) => {
    setSelectedOption(e.target.value);
    if (e.target.value === "model") {
      // Fetch the model's prediction here (not implemented in this example)
      // You may use an API call or other methods to get the model's prediction
      const modelPrediction = "Model's predicted value";
      setInputValue(modelPrediction);
    } else {
      setInputValue("");
    }
  };

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <div>
      <select value={selectedOption} onChange={handleOptionChange}>
        <option value="">Select Prediction Type</option>
        <option value="your">Your Prediction</option>
        <option value="model">Model Prediction</option>
      </select>
      <br />
      {selectedOption && (
        <input
          type="text"
          value={inputValue}
          onChange={handleInputChange}
          disabled={selectedOption === "model"}
          placeholder={
            selectedOption === "model"
              ? "Model's prediction will appear here"
              : "Enter your prediction"
          }
        />
      )}
    </div>
  );
};

export default PredictionInput;
