function randomNumber(min, max) {
    return (Math.floor(Math.random() * (max - min) + min))
}

function getRandomIntegerFromArray(array) {
    if (array.length === 0) {
        return undefined; // Return undefined if the array is empty
    }

    const randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
}

function getRandomNumbers() {
    const randomNumbers = [];

    while (randomNumbers.length < 5) {
        const randomNumber = Math.floor(Math.random() * 10);

        if (!randomNumbers.includes(randomNumber)) {
            randomNumbers.push(randomNumber);
        }
    }

    return randomNumbers;
}

export const MeemmaseedBest = (nums) => {
    const best = [];
    const random = [];
    for (let i = 0; i < nums.length; i++) {
        random.push(randomNumber(-5, 5))
    }
    for (let i = 0; i < nums.length; i++) {
        best.push({
            optimal: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + random[i]),
            optimalRandom: random[i],
            modalError: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z),
            modalErrorRandom: 0
        })
    }
    return best;
}

export const VussanutBest = (nums) => {
    const best = [];
    const random = [];
    for (let i = 0; i < nums.length; i++) {
        random.push(randomNumber(-5, 5));
    }
    for (let i = 0; i < nums.length; i++) {
        best.push({
            optimal: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + random[i]),
            optimalRandom: random[i],
            modalError: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z),
            modalErrorRandom: 0
            /*0.15 * sunshine + 0.55 * temperature - 0.3 * wind*/
        })
    }
    return best;
}

export const MeemmaseedBPRare = (nums) => {
    const random = []
    for (let i = 0; i < 2; i++) {
        random.push(Math.floor(Math.random() * 10))
    }
    const rare = []
    const randomerror = [];
    for (let i = 0; i < random.length; i++) {
        randomerror.push(randomNumber(-27, 27));
    }
    for (let i = 0; i < nums.length; i++) {
        if (i === random[0]) {
            // pushing 20% between (-27 to 27)
            rare.push({
                optimal: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + randomerror[0]),
                optimalRandom: randomerror[0],
                modalError: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z),
                modalErrorRandom: 0
            })
        } else if (i === random[1]) {
            rare.push({
                optimal: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + randomerror[1]),
                optimalRandom: randomerror[1],
                modalError: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z),
                modalErrorRandom: 0
            })
        } else {
            // pushing 80% as 0
            rare.push({
                optimal: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z),
                optimalRandom: 0,
                modalError: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z),
                modalErrorRandom: 0
            })
        }
    }
    return rare;
}

export const VussanutBPRare = (nums) => {
    const random = []
    for (let i = 0; i < 2; i++) {
        random.push(Math.floor(Math.random() * 10))
    }
    const rare = []
    const randomerror = [];
    for (let i = 0; i < random.length; i++) {
        randomerror.push(randomNumber(-27, 27))
    }
    for (let i = 0; i < nums.length; i++) {
        if (i === random[0]) {
            // pushing 20% between (-27 to 27)
            rare.push({
                optimal: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + randomerror[0]),
                optimalRandom: randomerror[0],
                modalError: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z),
                modalErrorRandom: 0
            })
        } else if (i === random[1]) {
            rare.push({
                optimal: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + randomerror[1]),
                optimalRandom: randomerror[1],
                modalError: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z),
                modalErrorRandom: 0
            })
        } else {
            // pushing 80% as 0
            rare.push({
                optimal: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z),
                optimalRandom: 0,
                modalError: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z),
                modalErrorRandom: 0
            })
        }
    }
    return rare;
}

export const MeemmaseedCont = (nums) => {
    // Optimal Continuous Artificial Error =  0.1 * sunshine + 0.9 * wind + randomerror{with probability 20% between [-27,27] and with probability 80% value = 0}
    const random = [] // for two random indexes
    for (let i = 0; i < 2; i++) {
        random.push(Math.floor(Math.random() * 10))
    }
    const randomerror = []; // for random values between -27 t0 27 for those indexes
    for (let i = 0; i < nums.length; i++) {
        if (i === random[0] || i === random[1]) {
            const error = randomNumber(-27, 27);
            randomerror.push(error)
        } else {
            randomerror.push(0)
        }
    }
    const ModalError = []; // for random values from these lists if random values drawn above is < 0, > 0 or = 0
    const list1 = [10, 11, 12, 13, 14];
    const list2 = [-10, -11, -12, -13, -14];
    const list3 = [-10, -11, -12, -13, -14, 10, 11, 12, 13, 14];
    for (let i = 0; i < nums.length; i++) {
        if (randomerror[i] < 0) {
            const val = getRandomIntegerFromArray(list1);
            ModalError.push(val);
        } else if (randomerror[i] > 0) {
            const val = getRandomIntegerFromArray(list2);
            ModalError.push(val);
        } else {
            const val = getRandomIntegerFromArray(list3);
            ModalError.push(val);
        }
    }

    const cont = []; // for list of objects with optimal and modalError (total count = 10)
    for (let i = 0; i < nums.length; i++) {
        cont.push({
            optimal: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + randomerror[i]),
            optimalRandom: randomerror[i],
            modalError: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + ModalError[i]),
            modalErrorRandom: ModalError[i]
        })
    }

    return cont;
}

export const VussanutCont = (nums) => {
    // Optimal Continuous Artificial Error =  0.1 * sunshine + 0.9 * wind + randomerror{with probability 20% between [-27,27] and with probability 80% value = 0}
    const random = [] // for two random indexes
    for (let i = 0; i < 2; i++) {
        random.push(Math.floor(Math.random() * 10))
    }
    const randomerror = []; // for random values between -27 t0 27 for those indexes
    for (let i = 0; i < nums.length; i++) {
        if (i === random[0] || i === random[1]) {
            const error = randomNumber(-27, 27);
            randomerror.push(error)
        } else {
            randomerror.push(0);
        }
    }
    const ModalError = []; // for random values from these lists if random values drawn above is < 0, > 0 or = 0
    const list1 = [13, 14, 15, 16, 17];
    const list2 = [-13, -14, -15, -16, -17];
    const list3 = [-13, -14, -15, -16, -17, 13, 14, 15, 16, 17];
    for (let i = 0; i < nums.length; i++) {
        if (randomerror[i] < 0) {
            const val = getRandomIntegerFromArray(list1);
            ModalError.push(val);
        } else if (randomerror[i] > 0) {
            const val = getRandomIntegerFromArray(list2);
            ModalError.push(val);
        } else {
            const val = getRandomIntegerFromArray(list3);
            ModalError.push(val);
        }
    }

    const cont = []; // for list of objects with optimal and modalError (total count = 10)
    for (let i = 0; i < nums.length; i++) {
        cont.push({
            optimal: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + randomerror[i]),
            optimalRandom: randomerror[i],
            modalError: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + ModalError[i]),
            modalErrorRandom: ModalError[i]
        })
    }

    return cont;
}

export const MeemmaseedRare = (nums) => {
    const randomerror = [];
    for (let i = 0; i < nums.length; i++) {
        randomerror.push(randomNumber(-5, 5))
    }
    const numbers = getRandomNumbers();
    const bprare = [];

    const value = [];

    for (let i = 0; i < nums.length; i++) {
        if (numbers.includes(i)) {
            if (randomerror[i] < 0) {
                value.push(24)
            } else if (randomerror[i] > 0) {
                value.push(-24)
            } else {
                value.push(randomNumber(-24, 24))
            }
        } else {
            value.push(0)
        }
    }

    for (let i = 0; i < nums.length; i++) {
        bprare.push({
            optimal: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + randomerror[i]),
            optimalRandom: randomerror[i],
            modalError: Math.floor(0.1 * nums[i].x + 0.9 * nums[i].z + value[i]),
            modalErrorRandom: value[i]
        })
    }

    return bprare;
}

export const VussanutRare = (nums) => {
    const randomerror = [];
    for (let i = 0; i < nums.length; i++) {
        randomerror.push(randomNumber(-5, 5))
    }
    const numbers = getRandomNumbers();
    const bprare = [];

    const values = [];

    for (let i = 0; i < nums.length; i++) {
        if (numbers.includes(i)) {
            if (randomerror[i] < 0) {
                values.push(30)
            } else if (randomerror[i] > 0) {
                values.push(-30)
            } else {
                values.push(randomNumber(-30, 30))
            }
        } else {
            values.push(0);
        }
    }

    for (let i = 0; i < nums.length; i++) {
        bprare.push({
            optimal: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + randomerror[i]),
            optimalRandom: randomerror[i],
            modalError: Math.floor(0.15 * nums[i].x + 0.55 * nums[i].y - 0.3 * nums[i].z + values[i]),
            modalErrorRandom: values[i],
        })
    }

    return bprare;
}



export const treatment1 = (nums) => {
    const membest = MeemmaseedBest(nums);
    const vusbest = VussanutBest(nums);

    const t1 = [];

    for (let i = 0; i < nums.length; i++) {
        t1.push({
            MeemmaseedOptimal: membest[i].optimal,
            MeemmaseedOptimalRandom: membest[i].optimalRandom,
            MeemmaseedModalError: membest[i].modalError,
            MeemmaseedModalErrorRandom: membest[i].modalErrorRandom,
            VussanutOptimal: vusbest[i].optimal,
            VussanutOptimalRandom: vusbest[i].optimalRandom,
            VussanutModalError: vusbest[i].modalError,
            VussanutModalErrorRandom: vusbest[i].modalErrorRandom
        })
    }

    return t1;
}

export const treatment2 = (nums) => {
    const memrare = MeemmaseedRare(nums);
    const vusbest = VussanutBest(nums);

    const t2 = [];

    for (let i = 0; i < nums.length; i++) {
        t2.push({
            MeemmaseedOptimal: memrare[i].optimal,
            MeemmaseedOptimalRandom: memrare[i].optimalRandom,
            MeemmaseedModalError: memrare[i].modalError,
            MeemmaseedModalErrorRandom: memrare[i].modalErrorRandom,
            VussanutOptimal: vusbest[i].optimal,
            VussanutOptimalRandom: vusbest[i].optimalRandom,
            VussanutModalError: vusbest[i].modalError,
            VussanutModalErrorRandom: vusbest[i].modalErrorRandom
        })
    }

    return t2;
}

export const treatment3 = (nums) => {
    const membest = MeemmaseedBest(nums);
    const vusrare = VussanutRare(nums);

    const t3 = [];

    for (let i = 0; i < nums.length; i++) {
        t3.push({
            MeemmaseedOptimal: membest[i].optimal,
            MeemmaseedOptimalRandom: membest[i].optimalRandom,
            MeemmaseedModalError: membest[i].modalError,
            MeemmaseedModalErrorRandom: membest[i].modalErrorRandom,
            VussanutOptimal: vusrare[i].optimal,
            VussanutOptimalRandom: vusrare[i].optimalRandom,
            VussanutModalError: vusrare[i].modalError,
            VussanutModalErrorRandom: vusrare[i].modalErrorRandom
        })
    }

    return t3;
}

export const treatment4 = (nums) => {
    const membprare = MeemmaseedBPRare(nums);
    const vusbprare = VussanutBPRare(nums);

    const t4 = [];

    for (let i = 0; i < nums.length; i++) {
        t4.push({
            MeemmaseedOptimal: membprare[i].optimal,
            MeemmaseedOptimalRandom: membprare[i].optimalRandom,
            MeemmaseedModalError: membprare[i].modalError,
            MeemmaseedModalErrorRandom: membprare[i].modalErrorRandom,
            VussanutOptimal: vusbprare[i].optimal,
            VussanutOptimalRandom: vusbprare[i].optimalRandom,
            VussanutModalError: vusbprare[i].modalError,
            VussanutModalErrorRandom: vusbprare[i].modalErrorRandom
        })
    }

    return t4;
}

export const treatment5 = (nums) => {
    const memcont = MeemmaseedCont(nums);
    const vusbprare = VussanutBPRare(nums);

    const t5 = [];

    for (let i = 0; i < nums.length; i++) {
        t5.push({
            MeemmaseedOptimal: memcont[i].optimal,
            MeemmaseedOptimalRandom: memcont[i].optimalRandom,
            MeemmaseedModalError: memcont[i].modalError,
            MeemmaseedModalErrorRandom: memcont[i].modalErrorRandom,
            VussanutOptimal: vusbprare[i].optimal,
            VussanutOptimalRandom: vusbprare[i].optimalRandom,
            VussanutModalError: vusbprare[i].modalError,
            VussanutModalErrorRandom: vusbprare[i].modalErrorRandom
        })
    }

    return t5;
}

export const treatment6 = (nums) => {
    const membprare = MeemmaseedBPRare(nums);
    const vuscont = VussanutCont(nums);

    const t6 = [];

    for (let i = 0; i < nums.length; i++) {
        t6.push({
            MeemmaseedOptimal: membprare[i].optimal,
            MeemmaseedOptimalRandom: membprare[i].optimalRandom,
            MeemmaseedModalError: membprare[i].modalError,
            MeemmaseedModalErrorRandom: membprare[i].modalErrorRandom,
            VussanutOptimal: vuscont[i].optimal,
            VussanutOptimalRandom: vuscont[i].optimalRandom,
            VussanutModalError: vuscont[i].modalError,
            VussanutModalErrorRandom: vuscont[i].modalErrorRandom
        })
    }

    return t6;
}