const treatment1 = [
    {
        Sunshine: [1, 3, 4, 4, 12],
        Temperature: [35, 62, 94, 82, 91],
        Wind: [20, 33, 20, 22, 61],
        Meemmaseed_Optimal_Irrigation: [18, 29, 15, 15, 56],
        Meemmaseed_System_Error: [18, 30, 18, 20, 56],
        Meemmaseed_Model_Error: [0, 1, 3, 5, 0],
        Vussanut_Optimal_Irrigation: [8, 30, 47, 35, 38],
        Vussanut_System_Error: [13, 25, 46, 39, 34],
        Vussanut_Model_Error: [5, 5, 1, 4, 4],
    },//1
    {
        Sunshine: [16, 13, 17, 14, 6],
        Temperature: [39, 65, 103, 96, 88],
        Wind: [6, 11, 54, 26, 38],
        Meemmaseed_Optimal_Irrigation: [3, 11, 53, 23, 39],
        Meemmaseed_System_Error: [7, 11, 50, 25, 35],
        Meemmaseed_Model_Error: [4, 0, 3, 2, 4],
        Vussanut_Optimal_Irrigation: [23, 39, 44, 45, 39],
        Vussanut_System_Error: [22, 34, 43, 47, 38],
        Vussanut_Model_Error: [1, 5, 1, 2, 1],
    },//2
    {
        Sunshine: [18, 13, 1, 12, 17],
        Temperature: [84, 106, 37, 67, 83],
        Wind: [40, 38, 5, 45, 57],
        Meemmaseed_Optimal_Irrigation: [38, 37, 1, 47, 55],
        Meemmaseed_System_Error: [38, 36, 5, 42, 53],
        Meemmaseed_Model_Error: [0, 1, 4, 5, 2],
        Vussanut_Optimal_Irrigation: [35, 49, 17, 22, 31],
        Vussanut_System_Error: [37, 49, 19, 25, 31],
        Vussanut_Model_Error: [2, 0, 2, 3, 0],
    },//3
    {
        Sunshine: [3, 4, 5, 9, 18],
        Temperature: [47, 92, 48, 88, 62],
        Wind: [52, 60, 52, 42, 27],
        Meemmaseed_Optimal_Irrigation: [43, 54, 42, 41, 23],
        Meemmaseed_System_Error: [47, 54, 47, 39, 26],
        Meemmaseed_Model_Error: [4, 0, 5, 2, 3],
        Vussanut_Optimal_Irrigation: [7, 38, 7, 34, 33],
        Vussanut_System_Error: [11, 33, 12, 37, 29],
        Vussanut_Model_Error: [4, 5, 5, 3, 4],
    },//4
    {
        Sunshine: [4, 15, 16, 18, 2],
        Temperature: [79, 83, 104, 36, 63],
        Wind: [8, 55, 6, 45, 21],
        Meemmaseed_Optimal_Irrigation: [10, 50, 4, 41, 17],
        Meemmaseed_System_Error: [8, 51, 7, 42, 19],
        Meemmaseed_Model_Error: [2, 1, 3, 1, 2],
        Vussanut_Optimal_Irrigation: [43, 35, 55, 9, 26],
        Vussanut_System_Error: [42, 31, 58, 9, 29],
        Vussanut_Model_Error: [1, 4, 3, 0, 3],
    },//5
    {
        Sunshine: [6, 14, 15, 9, 16],
        Temperature: [47, 105, 65, 90, 102],
        Wind: [23, 10, 54, 16, 60],
        Meemmaseed_Optimal_Irrigation: [16, 13, 54, 16, 53],
        Meemmaseed_System_Error: [21, 10, 50, 15, 56],
        Meemmaseed_Model_Error: [5, 3, 4, 1, 3],
        Vussanut_Optimal_Irrigation: [15, 56, 27, 49, 42],
        Vussanut_System_Error: [20, 57, 22, 46, 41],
        Vussanut_Model_Error: [5, 1, 5, 3, 1],
    },//6
    {
        Sunshine: [3, 13, 12, 4, 8],
        Temperature: [45, 37, 33, 102, 104],
        Wind: [47, 42, 45, 57, 60],
        Meemmaseed_Optimal_Irrigation: [42, 37, 47, 52, 52],
        Meemmaseed_System_Error: [43, 39, 42, 51, 55],
        Meemmaseed_Model_Error: [1, 2, 5, 1, 3],
        Vussanut_Optimal_Irrigation: [10, 11, 10, 42, 37],
        Vussanut_System_Error: [11, 10, 6, 40, 40],
        Vussanut_Model_Error: [1, 1, 4, 2, 3],
    },//7
    {
        Sunshine: [7, 7, 7, 2, 16],
        Temperature: [98, 63, 108, 65, 108],
        Wind: [43, 7, 61, 56, 49],
        Meemmaseed_Optimal_Irrigation: [42, 4, 61, 50, 50],
        Meemmaseed_System_Error: [39, 7, 56, 51, 46],
        Meemmaseed_Model_Error: [3, 3, 5, 1, 4],
        Vussanut_Optimal_Irrigation: [43, 38, 40, 22, 43],
        Vussanut_System_Error: [42, 34, 42, 19, 47],
        Vussanut_Model_Error: [1, 4, 2, 3, 4],
    },//8
    {
        Sunshine: [3, 14, 12, 11, 13],
        Temperature: [53, 32, 103, 49, 97],
        Wind: [16, 49, 39, 39, 44],
        Meemmaseed_Optimal_Irrigation: [11, 47, 31, 41, 42],
        Meemmaseed_System_Error: [15, 46, 36, 36, 41],
        Meemmaseed_Model_Error: [4, 1, 5, 5, 1],
        Vussanut_Optimal_Irrigation: [22, 4, 44, 19, 40],
        Vussanut_System_Error: [25, 5, 47, 17, 42],
        Vussanut_Model_Error: [3, 1, 3, 2, 2],
    },//9
    {
        Sunshine: [15, 2, 4, 6, 17],
        Temperature: [53, 68, 51, 97, 75],
        Wind: [22, 31, 11, 17, 60],
        Meemmaseed_Optimal_Irrigation: [17, 25, 8, 16, 57],
        Meemmaseed_System_Error: [21, 28, 10, 16, 56],
        Meemmaseed_Model_Error: [4, 3, 2, 0, 1],
        Vussanut_Optimal_Irrigation: [23, 28, 24, 54, 29],
        Vussanut_System_Error: [25, 28, 25, 49, 26],
        Vussanut_Model_Error: [2, 0, 1, 5, 3]
    }//10
];

const treatment2 = [
    {
        Sunshine: [1, 3, 4, 4, 12],
        Temperature: [35, 62, 94, 82, 91],
        Wind: [20, 33, 20, 22, 61],
        Meemmaseed_Optimal_Irrigation: [18, 29, 15, 15, 56],
        Meemmaseed_System_Error: [48, 30, 48, 20, 56],
        Meemmaseed_Model_Error: [30, 1, 33, 5, 0],
        Vussanut_Optimal_Irrigation: [8, 30, 47, 35, 38],
        Vussanut_System_Error: [13, 25, 46, 39, 34],
        Vussanut_Model_Error: [5, 5, 1, 4, 4],
    }, //1
    {
        Sunshine: [16, 13, 17, 14, 6],
        Temperature: [39, 65, 103, 96, 88],
        Wind: [6, 11, 54, 26, 38],
        Meemmaseed_Optimal_Irrigation: [3, 11, 53, 23, 39],
        Meemmaseed_System_Error: [37, 11, 20, 55, 35],
        Meemmaseed_Model_Error: [34, 0, 33, 32, 4],
        Vussanut_Optimal_Irrigation: [23, 39, 44, 45, 39],
        Vussanut_System_Error: [22, 34, 43, 47, 38],
        Vussanut_Model_Error: [1, 5, 1, 2, 1],
    },//2
    {
        Sunshine: [18, 13, 1, 12, 17],
        Temperature: [84, 106, 37, 67, 83],
        Wind: [40, 38, 5, 45, 57],
        Meemmaseed_Optimal_Irrigation: [38, 37, 1, 47, 55],
        Meemmaseed_System_Error: [38, 6, 35, 42, 53],
        Meemmaseed_Model_Error: [0, 31, 34, 5, 2],
        Vussanut_Optimal_Irrigation: [35, 49, 17, 22, 31],
        Vussanut_System_Error: [37, 49, 19, 25, 31],
        Vussanut_Model_Error: [2, 0, 2, 3, 0],
    },//3
    {
        Sunshine: [3, 4, 5, 9, 18],
        Temperature: [47, 92, 48, 88, 62],
        Wind: [52, 60, 52, 42, 27],
        Meemmaseed_Optimal_Irrigation: [43, 54, 42, 41, 23],
        Meemmaseed_System_Error: [47, 54, 47, 39, 26],
        Meemmaseed_Model_Error: [4, 0, 5, 2, 3],
        Vussanut_Optimal_Irrigation: [7, 38, 7, 34, 33],
        Vussanut_System_Error: [11, 33, 12, 37, 29],
        Vussanut_Model_Error: [4, 5, 5, 3, 4],
    },//4
    {
        Sunshine: [4, 15, 16, 18, 2],
        Temperature: [79, 83, 104, 36, 63],
        Wind: [8, 55, 6, 45, 21],
        Meemmaseed_Optimal_Irrigation: [10, 50, 4, 41, 17],
        Meemmaseed_System_Error: [-22, 81, 40, 72, 49],
        Meemmaseed_Model_Error: [32, 31, 33, 31, 32],
        Vussanut_Optimal_Irrigation: [43, 35, 55, 9, 26],
        Vussanut_System_Error: [42, 31, 58, 9, 29],
        Vussanut_Model_Error: [1, 4, 3, 0, 3],
    },//5
    {
        Sunshine: [6, 14, 15, 9, 16],
        Temperature: [47, 105, 65, 90, 102],
        Wind: [23, 10, 54, 16, 60],
        Meemmaseed_Optimal_Irrigation: [16, 13, 54, 16, 53],
        Meemmaseed_System_Error: [51, 10, 20, -15, 56],
        Meemmaseed_Model_Error: [35, 3, 34, 31, 3],
        Vussanut_Optimal_Irrigation: [15, 56, 27, 49, 42],
        Vussanut_System_Error: [20, 57, 22, 46, 41],
        Vussanut_Model_Error: [5, 1, 5, 3, 1],
    },//6
    {
        Sunshine: [3, 13, 12, 4, 8],
        Temperature: [45, 37, 33, 102, 104],
        Wind: [47, 42, 45, 57, 60],
        Meemmaseed_Optimal_Irrigation: [42, 37, 47, 52, 52],
        Meemmaseed_System_Error: [43, 39, 12, 21, 85],
        Meemmaseed_Model_Error: [1, 2, 35, 31, 33],
        Vussanut_Optimal_Irrigation: [10, 11, 10, 42, 37],
        Vussanut_System_Error: [11, 10, 6, 40, 40],
        Vussanut_Model_Error: [1, 1, 4, 2, 3],
    },//7
    {
        Sunshine: [7, 7, 7, 2, 16],
        Temperature: [98, 63, 108, 65, 108],
        Wind: [43, 7, 61, 56, 49],
        Meemmaseed_Optimal_Irrigation: [42, 4, 61, 50, 50],
        Meemmaseed_System_Error: [9, 7, 56, 51, 16],
        Meemmaseed_Model_Error: [33, 3, 5, 1, 34],
        Vussanut_Optimal_Irrigation: [43, 38, 40, 22, 43],
        Vussanut_System_Error: [42, 34, 42, 19, 47],
        Vussanut_Model_Error: [1, 4, 2, 3, 4],
    },//8
    {
        Sunshine: [3, 14, 12, 11, 13],
        Temperature: [53, 32, 103, 49, 97],
        Wind: [16, 49, 39, 39, 44],
        Meemmaseed_Optimal_Irrigation: [11, 47, 31, 41, 42],
        Meemmaseed_System_Error: [15, 16, 66, 36, 41],
        Meemmaseed_Model_Error: [4, 31, 35, 5, 1],
        Vussanut_Optimal_Irrigation: [22, 4, 44, 19, 40],
        Vussanut_System_Error: [25, 5, 47, 17, 42],
        Vussanut_Model_Error: [3, 1, 3, 2, 2],
    },//9
    {
        Sunshine: [15, 2, 4, 6, 17],
        Temperature: [53, 68, 51, 97, 75],
        Wind: [22, 31, 11, 17, 60],
        Meemmaseed_Optimal_Irrigation: [17, 25, 8, 16, 57],
        Meemmaseed_System_Error: [51, 28, 40, -14, 56],
        Meemmaseed_Model_Error: [34, 3, 32, 30, 1],
        Vussanut_Optimal_Irrigation: [23, 28, 24, 54, 29],
        Vussanut_System_Error: [25, 28, 25, 49, 26],
        Vussanut_Model_Error: [2, 0, 1, 5, 3]
    }//10
];

const treatment3 = [
    {
        Sunshine: [1, 3, 4, 4, 12],
        Temperature: [35, 62, 94, 82, 91],
        Wind: [20, 33, 20, 22, 61],
        Meemmaseed_Optimal_Irrigation: [18, 29, 15, 15, 56],
        Meemmaseed_System_Error: [18, 30, 18, 20, 56],
        Meemmaseed_Model_Error: [0, 1, 3, 5, 0],
        Vussanut_Optimal_Irrigation: [8, 30, 47, 35, 38],
        Vussanut_System_Error: [43, 25, 46, 69, 34],
        Vussanut_Model_Error: [35, 5, 1, 34, 4],
    }, //1
    {
        Sunshine: [16, 13, 17, 14, 6],
        Temperature: [39, 65, 103, 96, 88],
        Wind: [6, 11, 54, 26, 38],
        Meemmaseed_Optimal_Irrigation: [3, 11, 53, 23, 39],
        Meemmaseed_System_Error: [7, 11, 50, 25, 35],
        Meemmaseed_Model_Error: [4, 0, 3, 2, 4],
        Vussanut_Optimal_Irrigation: [23, 39, 44, 45, 39],
        Vussanut_System_Error: [22, 34, 13, 77, 38],
        Vussanut_Model_Error: [1, 5, 31, 32, 1],
    },//2
    {
        Sunshine: [18, 13, 1, 12, 17],
        Temperature: [84, 106, 37, 67, 83],
        Wind: [40, 38, 5, 45, 57],
        Meemmaseed_Optimal_Irrigation: [38, 37, 1, 47, 55],
        Meemmaseed_System_Error: [38, 36, 5, 42, 53],
        Meemmaseed_Model_Error: [0, 1, 4, 5, 2],
        Vussanut_Optimal_Irrigation: [35, 49, 17, 22, 31],
        Vussanut_System_Error: [37, 19, 49, 55, 31],
        Vussanut_Model_Error: [2, 30, 32, 33, 0],
    },//3
    {
        Sunshine: [3, 4, 5, 9, 18],
        Temperature: [47, 92, 48, 88, 62],
        Wind: [52, 60, 52, 42, 27],
        Meemmaseed_Optimal_Irrigation: [43, 54, 42, 41, 23],
        Meemmaseed_System_Error: [47, 54, 47, 39, 26],
        Meemmaseed_Model_Error: [4, 0, 5, 2, 3],
        Vussanut_Optimal_Irrigation: [7, 38, 7, 34, 33],
        Vussanut_System_Error: [41, 3, 12, 67, 29],
        Vussanut_Model_Error: [34, 35, 5, 33, 4],
    },//4
    {
        Sunshine: [4, 15, 16, 18, 2],
        Temperature: [79, 83, 104, 36, 63],
        Wind: [8, 55, 6, 45, 21],
        Meemmaseed_Optimal_Irrigation: [10, 50, 4, 41, 17],
        Meemmaseed_System_Error: [8, 51, 7, 42, 19],
        Meemmaseed_Model_Error: [2, 1, 3, 1, 2],
        Vussanut_Optimal_Irrigation: [43, 35, 55, 9, 26],
        Vussanut_System_Error: [12, 31, 58, 9, 59],
        Vussanut_Model_Error: [31, 4, 3, 0, 33],
    },//5
    {
        Sunshine: [6, 14, 15, 9, 16],
        Temperature: [47, 105, 65, 90, 102],
        Wind: [23, 10, 54, 16, 60],
        Meemmaseed_Optimal_Irrigation: [16, 13, 54, 16, 53],
        Meemmaseed_System_Error: [21, 10, 50, 15, 56],
        Meemmaseed_Model_Error: [5, 3, 4, 1, 3],
        Vussanut_Optimal_Irrigation: [15, 56, 27, 49, 42],
        Vussanut_System_Error: [20, 57, 22, 46, 41],
        Vussanut_Model_Error: [5, 1, 5, 3, 1],
    },//6
    {
        Sunshine: [3, 13, 12, 4, 8],
        Temperature: [45, 37, 33, 102, 104],
        Wind: [47, 42, 45, 57, 60],
        Meemmaseed_Optimal_Irrigation: [42, 37, 47, 52, 52],
        Meemmaseed_System_Error: [43, 39, 42, 51, 55],
        Meemmaseed_Model_Error: [1, 2, 5, 1, 3],
        Vussanut_Optimal_Irrigation: [10, 11, 10, 42, 37],
        Vussanut_System_Error: [41, -20, -24, 10, 70],
        Vussanut_Model_Error: [31, 31, 34, 32, 33],
    },//7
    {
        Sunshine: [7, 7, 7, 2, 16],
        Temperature: [98, 63, 108, 65, 108],
        Wind: [43, 7, 61, 56, 49],
        Meemmaseed_Optimal_Irrigation: [42, 4, 61, 50, 50],
        Meemmaseed_System_Error: [39, 7, 56, 51, 46],
        Meemmaseed_Model_Error: [3, 3, 5, 1, 4],
        Vussanut_Optimal_Irrigation: [43, 38, 40, 22, 43],
        Vussanut_System_Error: [12, 4, 42, 19, 47],
        Vussanut_Model_Error: [31, 34, 2, 3, 4],
    },//8
    {
        Sunshine: [3, 14, 12, 11, 13],
        Temperature: [53, 32, 103, 49, 97],
        Wind: [16, 49, 39, 39, 44],
        Meemmaseed_Optimal_Irrigation: [11, 47, 31, 41, 42],
        Meemmaseed_System_Error: [15, 46, 36, 36, 41],
        Meemmaseed_Model_Error: [4, 1, 5, 5, 1],
        Vussanut_Optimal_Irrigation: [22, 4, 44, 19, 40],
        Vussanut_System_Error: [55, 35, 47, 17, 72],
        Vussanut_Model_Error: [33, 31, 3, 2, 32],
    },//9
    {
        Sunshine: [15, 2, 4, 6, 17],
        Temperature: [53, 68, 51, 97, 75],
        Wind: [22, 31, 11, 17, 60],
        Meemmaseed_Optimal_Irrigation: [17, 25, 8, 16, 57],
        Meemmaseed_System_Error: [21, 28, 10, 16, 56],
        Meemmaseed_Model_Error: [4, 3, 2, 0, 1],
        Vussanut_Optimal_Irrigation: [23, 28, 24, 54, 29],
        Vussanut_System_Error: [55, 28, 55, 19, 26],
        Vussanut_Model_Error: [32, 0, 31, 35, 3]
    }//10
];

const treatment4 = [
    {
        Sunshine: [1, 3, 4, 4, 12],
        Temperature: [35, 62, 94, 82, 91],
        Wind: [20, 33, 20, 22, 61],
        Meemmaseed_Optimal_Irrigation: [18, 30, 26, 20, 56],
        Meemmaseed_System_Error: [18, 30, 18, 20, 56],
        Meemmaseed_Model_Error: [0, 0, 8, 0, 0],
        Vussanut_Optimal_Irrigation: [13, 25, 46, 39, 34],
        Vussanut_System_Error: [13, 25, 46, 39, 34],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    }, //1
    {
        Sunshine: [16, 13, 17, 14, 6],
        Temperature: [39, 65, 103, 96, 88],
        Wind: [6, 11, 54, 26, 38],
        Meemmaseed_Optimal_Irrigation: [7, 11, 50, 25, 35],
        Meemmaseed_System_Error: [7, 11, 50, 25, 35],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [40, 18, 64, 51, 23],
        Vussanut_System_Error: [26, 34, 43, 47, 38],
        Vussanut_Model_Error: [14, 16, 21, 4, 15],
    },//2
    {
        Sunshine: [18, 13, 1, 12, 17],
        Temperature: [84, 106, 37, 67, 83],
        Wind: [40, 38, 5, 45, 57],
        Meemmaseed_Optimal_Irrigation: [38, 36, 5, 42, 53],
        Meemmaseed_System_Error: [38, 36, 5, 42, 53],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [37, 49, 27, 25, 31],
        Vussanut_System_Error: [37, 49, 19, 25, 31],
        Vussanut_Model_Error: [0, 0, 8, 0, 0],
    },//3
    {
        Sunshine: [3, 4, 5, 9, 18],
        Temperature: [47, 92, 48, 88, 62],
        Wind: [52, 60, 52, 42, 27],
        Meemmaseed_Optimal_Irrigation: [60, 75, 62, 28, 7],
        Meemmaseed_System_Error: [47, 54, 47, 39, 26],
        Meemmaseed_Model_Error: [17, 21, 15, 11, 19],
        Vussanut_Optimal_Irrigation: [11, 33, 3, 37, 29],
        Vussanut_System_Error: [11, 33, 12, 37, 29],
        Vussanut_Model_Error: [0, 0, 9, 0, 0],
    },//4
    {
        Sunshine: [4, 15, 16, 18, 2],
        Temperature: [79, 83, 104, 36, 63],
        Wind: [8, 55, 6, 45, 21],
        Meemmaseed_Optimal_Irrigation: [8, 51, 7, 42, 19],
        Meemmaseed_System_Error: [8, 51, 7, 42, 19],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [42, 31, 58, 9, 29],
        Vussanut_System_Error: [42, 31, 58, 9, 29],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    },//5
    {
        Sunshine: [6, 14, 15, 9, 16],
        Temperature: [47, 105, 65, 90, 102],
        Wind: [23, 10, 54, 16, 60],
        Meemmaseed_Optimal_Irrigation: [21, 10, 50, 15, 56],
        Meemmaseed_System_Error: [21, 10, 50, 15, 56],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [20, 57, 22, 46, 24],
        Vussanut_System_Error: [8, 35, 6, 51, 41],
        Vussanut_Model_Error: [12, 22, 16, 5, 17],
    },//6
    {
        Sunshine: [3, 13, 12, 4, 8],
        Temperature: [45, 37, 33, 102, 104],
        Wind: [47, 42, 45, 57, 60],
        Meemmaseed_Optimal_Irrigation: [43, 39, 51, 52, 55],
        Meemmaseed_System_Error: [43, 39, 42, 52, 55],
        Meemmaseed_Model_Error: [0, 0, 9, 0, 0],
        Vussanut_Optimal_Irrigation: [11, 10, 6, 40, 40],
        Vussanut_System_Error: [11, 10, 6, 40, 40],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    },//7
    {
        Sunshine: [7, 7, 7, 2, 16],
        Temperature: [98, 63, 108, 65, 108],
        Wind: [43, 7, 61, 56, 49],
        Meemmaseed_Optimal_Irrigation: [39, 7, 56, 51, 46],
        Meemmaseed_System_Error: [39, 7, 56, 51, 46],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [42, 34, 56, 19, 47],
        Vussanut_System_Error: [42, 34, 56, 19, 47],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    },//8
    {
        Sunshine: [3, 14, 12, 11, 13],
        Temperature: [53, 32, 103, 49, 97],
        Wind: [16, 49, 39, 39, 44],
        Meemmaseed_Optimal_Irrigation: [29, 25, 39, 19, 47],
        Meemmaseed_System_Error: [15, 46, 36, 36, 41],
        Meemmaseed_Model_Error: [14, 21, 3, 17, 6],
        Vussanut_Optimal_Irrigation: [25, 5, 31, 17, 42],
        Vussanut_System_Error: [25, 5, 47, 17, 42],
        Vussanut_Model_Error: [0, 0, 16, 0, 0],
    },//9
    {
        Sunshine: [15, 2, 4, 6, 17],
        Temperature: [53, 68, 51, 97, 75],
        Wind: [22, 31, 11, 17, 60],
        Meemmaseed_Optimal_Irrigation: [21, 28, 26, 16, 56],
        Meemmaseed_System_Error: [21, 28, 10, 16, 56],
        Meemmaseed_Model_Error: [0, 0, 16, 0, 0],
        Vussanut_Optimal_Irrigation: [25, 28, 25, 49, 26],
        Vussanut_System_Error: [25, 28, 25, 49, 26],
        Vussanut_Model_Error: [0, 0, 0, 0, 0]
    }//10
];

const treatment5 = [
    {
        Sunshine: [1, 3, 4, 4, 12],
        Temperature: [35, 62, 94, 82, 91],
        Wind: [20, 33, 20, 22, 61],
        Meemmaseed_Optimal_Irrigation: [18, 30, 26, 20, 56],
        Meemmaseed_System_Error: [5, 18, -2, 31, 70],
        Meemmaseed_Model_Error: [13, 12, 20, 11, 14],
        Vussanut_Optimal_Irrigation: [13, 25, 46, 39, 34],
        Vussanut_System_Error: [13, 25, 46, 39, 34],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    }, //1
    {
        Sunshine: [16, 13, 17, 14, 6],
        Temperature: [39, 65, 103, 96, 88],
        Wind: [6, 11, 54, 26, 38],
        Meemmaseed_Optimal_Irrigation: [7, 11, 50, 25, 35],
        Meemmaseed_System_Error: [21, 24, 39, 14, 48],
        Meemmaseed_Model_Error: [14, 13, 11, 11, 13],
        Vussanut_Optimal_Irrigation: [40, 18, 64, 51, 23],
        Vussanut_System_Error: [26, 34, 43, 47, 38],
        Vussanut_Model_Error: [14, 16, 21, 4, 15],
    },//2
    {
        Sunshine: [18, 13, 1, 12, 17],
        Temperature: [84, 106, 37, 67, 83],
        Wind: [40, 38, 5, 45, 57],
        Meemmaseed_Optimal_Irrigation: [38, 36, 5, 42, 53],
        Meemmaseed_System_Error: [48, 48, 15, 31, 39],
        Meemmaseed_Model_Error: [10, 12, 10, 11, 14],
        Vussanut_Optimal_Irrigation: [37, 49, 27, 25, 31],
        Vussanut_System_Error: [37, 49, 19, 25, 31],
        Vussanut_Model_Error: [0, 0, 8, 0, 0],
    },//3
    {
        Sunshine: [3, 4, 5, 9, 18],
        Temperature: [47, 92, 48, 88, 62],
        Wind: [52, 60, 52, 42, 27],
        Meemmaseed_Optimal_Irrigation: [60, 75, 62, 28, 7],
        Meemmaseed_System_Error: [31, 43, 33, 50, 37],
        Meemmaseed_Model_Error: [29, 32, 29, 22, 30],
        Vussanut_Optimal_Irrigation: [11, 33, 3, 37, 29],
        Vussanut_System_Error: [11, 33, 12, 37, 29],
        Vussanut_Model_Error: [0, 0, 9, 0, 0],
    },//4
    {
        Sunshine: [4, 15, 16, 18, 2],
        Temperature: [79, 83, 104, 36, 63],
        Wind: [8, 55, 6, 45, 21],
        Meemmaseed_Optimal_Irrigation: [8, 51, 7, 42, 19],
        Meemmaseed_System_Error: [20, 39, 20, 32, 7],
        Meemmaseed_Model_Error: [12, 12, 13, 10, 12],
        Vussanut_Optimal_Irrigation: [42, 31, 58, 9, 29],
        Vussanut_System_Error: [42, 31, 58, 9, 29],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    },//5
    {
        Sunshine: [6, 14, 15, 9, 16],
        Temperature: [47, 105, 65, 90, 102],
        Wind: [23, 10, 54, 16, 60],
        Meemmaseed_Optimal_Irrigation: [21, 10, 50, 15, 56],
        Meemmaseed_System_Error: [31, 24, 64, 2, 68],
        Meemmaseed_Model_Error: [10, 14, 14, 13, 12],
        Vussanut_Optimal_Irrigation: [20, 57, 22, 46, 24],
        Vussanut_System_Error: [8, 35, 6, 51, 41],
        Vussanut_Model_Error: [12, 22, 16, 5, 17],
    },//6
    {
        Sunshine: [3, 13, 12, 4, 8],
        Temperature: [45, 37, 33, 102, 104],
        Wind: [47, 42, 45, 57, 60],
        Meemmaseed_Optimal_Irrigation: [43, 39, 51, 52, 55],
        Meemmaseed_System_Error: [53, 29, 21, 42, 69],
        Meemmaseed_Model_Error: [10, 10, 21, 10, 14],
        Vussanut_Optimal_Irrigation: [11, 10, 6, 40, 40],
        Vussanut_System_Error: [11, 10, 6, 40, 40],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    },//7
    {
        Sunshine: [7, 7, 7, 2, 16],
        Temperature: [98, 63, 108, 65, 108],
        Wind: [43, 7, 61, 56, 49],
        Meemmaseed_Optimal_Irrigation: [39, 7, 56, 51, 46],
        Meemmaseed_System_Error: [29, 19, 43, 65, 36],
        Meemmaseed_Model_Error: [10, 12, 13, 14, 10],
        Vussanut_Optimal_Irrigation: [42, 34, 56, 19, 47],
        Vussanut_System_Error: [42, 34, 56, 19, 47],
        Vussanut_Model_Error: [0, 0, 0, 0, 0],
    },//8
    {
        Sunshine: [3, 14, 12, 11, 13],
        Temperature: [53, 32, 103, 49, 97],
        Wind: [16, 49, 39, 39, 44],
        Meemmaseed_Optimal_Irrigation: [29, 25, 39, 19, 47],
        Meemmaseed_System_Error: [3, 57, 22, 48, 31],
        Meemmaseed_Model_Error: [26, 32, 17, 29, 16],
        Vussanut_Optimal_Irrigation: [25, 5, 31, 17, 42],
        Vussanut_System_Error: [25, 5, 47, 17, 42],
        Vussanut_Model_Error: [0, 0, 16, 0, 0],
    },//9
    {
        Sunshine: [15, 2, 4, 6, 17],
        Temperature: [53, 68, 51, 97, 75],
        Wind: [22, 31, 11, 17, 60],
        Meemmaseed_Optimal_Irrigation: [21, 28, 26, 16, 56],
        Meemmaseed_System_Error: [8, 17, -4, 4, 70],
        Meemmaseed_Model_Error: [13, 11, 30, 12, 14],
        Vussanut_Optimal_Irrigation: [25, 28, 25, 49, 26],
        Vussanut_System_Error: [25, 28, 25, 49, 26],
        Vussanut_Model_Error: [0, 0, 0, 0, 0]
    }//10
];

const treatment6 = [
    {
        Sunshine: [1, 3, 4, 4, 12],
        Temperature: [35, 62, 94, 82, 91],
        Wind: [20, 33, 20, 22, 61],
        Meemmaseed_Optimal_Irrigation: [18, 30, 26, 20, 56],
        Meemmaseed_System_Error: [18, 30, 18, 20, 56],
        Meemmaseed_Model_Error: [0, 0, 8, 0, 0],
        Vussanut_Optimal_Irrigation: [13, 25, 46, 39, 34],
        Vussanut_System_Error: [0, 11, 39, 54, 19],
        Vussanut_Model_Error: [13, 14, 17, 15, 15],
    }, //1
    {
        Sunshine: [16, 13, 17, 14, 6],
        Temperature: [39, 65, 103, 96, 88],
        Wind: [6, 11, 54, 26, 38],
        Meemmaseed_Optimal_Irrigation: [7, 11, 50, 25, 35],
        Meemmaseed_System_Error: [7, 11, 50, 25, 35],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [40, 18, 64, 51, 23],
        Vussanut_System_Error: [11, 50, 27, 70, 55],
        Vussanut_Model_Error: [29, 32, 37, 19, 32],
    },//2
    {
        Sunshine: [18, 13, 1, 12, 17],
        Temperature: [84, 106, 37, 67, 83],
        Wind: [40, 38, 5, 45, 57],
        Meemmaseed_Optimal_Irrigation: [38, 36, 5, 42, 53],
        Meemmaseed_System_Error: [38, 36, 5, 42, 53],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [37, 49, 27, 25, 31],
        Vussanut_System_Error: [53, 32, 13, 12, 48],
        Vussanut_Model_Error: [16, 17, 14, 13, 17],
    },//3
    {
        Sunshine: [3, 4, 5, 9, 18],
        Temperature: [47, 92, 48, 88, 62],
        Wind: [52, 60, 52, 42, 27],
        Meemmaseed_Optimal_Irrigation: [60, 75, 62, 28, 7],
        Meemmaseed_System_Error: [47, 54, 47, 39, 26],
        Meemmaseed_Model_Error: [17, 21, 15, 11, 19],
        Vussanut_Optimal_Irrigation: [11, 33, 3, 37, 29],
        Vussanut_System_Error: [24, 20, 29, 54, 42],
        Vussanut_Model_Error: [13, 13, 26, 17, 13],
    },//4
    {
        Sunshine: [4, 15, 16, 18, 2],
        Temperature: [79, 83, 104, 36, 63],
        Wind: [8, 55, 6, 45, 21],
        Meemmaseed_Optimal_Irrigation: [8, 51, 7, 42, 19],
        Meemmaseed_System_Error: [8, 51, 7, 42, 19],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [42, 31, 58, 9, 29],
        Vussanut_System_Error: [59, 16, 71, 23, 14],
        Vussanut_Model_Error: [17, 15, 13, 14, 15],
    },//5
    {
        Sunshine: [6, 14, 15, 9, 16],
        Temperature: [47, 105, 65, 90, 102],
        Wind: [23, 10, 54, 16, 60],
        Meemmaseed_Optimal_Irrigation: [21, 10, 50, 15, 56],
        Meemmaseed_System_Error: [21, 10, 50, 15, 56],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [20, 57, 22, 46, 24],
        Vussanut_System_Error: [8, 35, -11, 68, 55],
        Vussanut_Model_Error: [-5, 21, 33, 22, 31],
    },//6
    {
        Sunshine: [3, 13, 12, 4, 8],
        Temperature: [45, 37, 33, 102, 104],
        Wind: [47, 42, 45, 57, 60],
        Meemmaseed_Optimal_Irrigation: [43, 39, 51, 52, 55],
        Meemmaseed_System_Error: [43, 39, 42, 52, 55],
        Meemmaseed_Model_Error: [0, 0, 9, 0, 0],
        Vussanut_Optimal_Irrigation: [11, 10, 6, 40, 40],
        Vussanut_System_Error: [27, 23, 19, 55, 57],
        Vussanut_Model_Error: [16, 13, 13, 15, 17],
    },//7
    {
        Sunshine: [7, 7, 7, 2, 16],
        Temperature: [98, 63, 108, 65, 108],
        Wind: [43, 7, 61, 56, 49],
        Meemmaseed_Optimal_Irrigation: [39, 7, 56, 51, 46],
        Meemmaseed_System_Error: [39, 7, 56, 51, 46],
        Meemmaseed_Model_Error: [0, 0, 0, 0, 0],
        Vussanut_Optimal_Irrigation: [42, 34, 56, 19, 47],
        Vussanut_System_Error: [58, 21, 39, 6, 63],
        Vussanut_Model_Error: [16, 13, 17, 13, 16],
    },//8
    {
        Sunshine: [3, 14, 12, 11, 13],
        Temperature: [53, 32, 103, 49, 97],
        Wind: [16, 49, 39, 39, 44],
        Meemmaseed_Optimal_Irrigation: [29, 25, 39, 19, 47],
        Meemmaseed_System_Error: [15, 46, 36, 36, 41],
        Meemmaseed_Model_Error: [14, 21, 3, 17, 6],
        Vussanut_Optimal_Irrigation: [25, 5, 31, 17, 42],
        Vussanut_System_Error: [41, 21, 64, 1, 59],
        Vussanut_Model_Error: [16, 16, 33, 16, 17],
    },//9
    {
        Sunshine: [15, 2, 4, 6, 17],
        Temperature: [53, 68, 51, 97, 75],
        Wind: [22, 31, 11, 17, 60],
        Meemmaseed_Optimal_Irrigation: [21, 28, 26, 16, 56],
        Meemmaseed_System_Error: [21, 28, 10, 16, 56],
        Meemmaseed_Model_Error: [0, 0, 16, 0, 0],
        Vussanut_Optimal_Irrigation: [25, 28, 25, 49, 26],
        Vussanut_System_Error: [11, 14, 9, 62, 40],
        Vussanut_Model_Error: [14, 14, 16, 13, 14]
    }//10
];

export const getTreatment1 = () => {
    const randomarray = [];
    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 5);
        randomarray.push(random);
    }

    let meemmaseedbest = [];

    for (let i = 0; i < 10; i++) {
        meemmaseedbest.push({
            Sunshine: treatment1[i].Sunshine[randomarray[i]],
            Temperature: treatment1[i].Temperature[randomarray[i]],
            Wind: treatment1[i].Wind[randomarray[i]],
            Meemmaseed_Optimal_Irrigation: treatment1[i].Meemmaseed_Optimal_Irrigation[randomarray[i]],
            Meemmaseed_System_Error: treatment1[i].Meemmaseed_System_Error[randomarray[i]],
            Meemmaseed_Model_Error: treatment1[i].Meemmaseed_Model_Error[randomarray[i]],
            Vussanut_Optimal_Irrigation: treatment1[i].Vussanut_Optimal_Irrigation[randomarray[i]],
            Vussanut_System_Error: treatment1[i].Vussanut_System_Error[randomarray[i]],
            Vussanut_Model_Error: treatment1[i].Vussanut_Model_Error[randomarray[i]]
        })
    }

    return meemmaseedbest;
}

export const getTreatment2 = () => {
    const randomarray = [];
    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 5);
        randomarray.push(random);
    }

    let meemmaseedbest = [];

    for (let i = 0; i < 10; i++) {
        meemmaseedbest.push({
            Sunshine: treatment2[i].Sunshine[randomarray[i]],
            Temperature: treatment2[i].Temperature[randomarray[i]],
            Wind: treatment2[i].Wind[randomarray[i]],
            Meemmaseed_Optimal_Irrigation: treatment2[i].Meemmaseed_Optimal_Irrigation[randomarray[i]],
            Meemmaseed_System_Error: treatment2[i].Meemmaseed_System_Error[randomarray[i]],
            Meemmaseed_Model_Error: treatment2[i].Meemmaseed_Model_Error[randomarray[i]],
            Vussanut_Optimal_Irrigation: treatment2[i].Vussanut_Optimal_Irrigation[randomarray[i]],
            Vussanut_System_Error: treatment2[i].Vussanut_System_Error[randomarray[i]],
            Vussanut_Model_Error: treatment2[i].Vussanut_Model_Error[randomarray[i]]
        })
    }

    return meemmaseedbest;
}

export const getTreatment3 = () => {
    const randomarray = [];
    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 5);
        randomarray.push(random);
    }

    let meemmaseedbest = [];

    for (let i = 0; i < 10; i++) {
        meemmaseedbest.push({
            Sunshine: treatment3[i].Sunshine[randomarray[i]],
            Temperature: treatment3[i].Temperature[randomarray[i]],
            Wind: treatment3[i].Wind[randomarray[i]],
            Meemmaseed_Optimal_Irrigation: treatment3[i].Meemmaseed_Optimal_Irrigation[randomarray[i]],
            Meemmaseed_System_Error: treatment3[i].Meemmaseed_System_Error[randomarray[i]],
            Meemmaseed_Model_Error: treatment3[i].Meemmaseed_Model_Error[randomarray[i]],
            Vussanut_Optimal_Irrigation: treatment3[i].Vussanut_Optimal_Irrigation[randomarray[i]],
            Vussanut_System_Error: treatment3[i].Vussanut_System_Error[randomarray[i]],
            Vussanut_Model_Error: treatment3[i].Vussanut_Model_Error[randomarray[i]]
        })
    }

    return meemmaseedbest;
}

export const getTreatment4 = () => {
    const randomarray = [];
    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 5);
        randomarray.push(random);
    }

    let meemmaseedbest = [];

    for (let i = 0; i < 10; i++) {
        meemmaseedbest.push({
            Sunshine: treatment4[i].Sunshine[randomarray[i]],
            Temperature: treatment4[i].Temperature[randomarray[i]],
            Wind: treatment4[i].Wind[randomarray[i]],
            Meemmaseed_Optimal_Irrigation: treatment4[i].Meemmaseed_Optimal_Irrigation[randomarray[i]],
            Meemmaseed_System_Error: treatment4[i].Meemmaseed_System_Error[randomarray[i]],
            Meemmaseed_Model_Error: treatment4[i].Meemmaseed_Model_Error[randomarray[i]],
            Vussanut_Optimal_Irrigation: treatment4[i].Vussanut_Optimal_Irrigation[randomarray[i]],
            Vussanut_System_Error: treatment4[i].Vussanut_System_Error[randomarray[i]],
            Vussanut_Model_Error: treatment4[i].Vussanut_Model_Error[randomarray[i]],
            Random: randomarray[i]
        })
    }

    return meemmaseedbest;
}

export const getTreatment5 = () => {
    const randomarray = [];
    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 5);
        randomarray.push(random);
    }

    let meemmaseedbest = [];

    for (let i = 0; i < 10; i++) {
        meemmaseedbest.push({
            Sunshine: treatment5[i].Sunshine[randomarray[i]],
            Temperature: treatment5[i].Temperature[randomarray[i]],
            Wind: treatment5[i].Wind[randomarray[i]],
            Meemmaseed_Optimal_Irrigation: treatment5[i].Meemmaseed_Optimal_Irrigation[randomarray[i]],
            Meemmaseed_System_Error: treatment5[i].Meemmaseed_System_Error[randomarray[i]],
            Meemmaseed_Model_Error: treatment5[i].Meemmaseed_Model_Error[randomarray[i]],
            Vussanut_Optimal_Irrigation: treatment5[i].Vussanut_Optimal_Irrigation[randomarray[i]],
            Vussanut_System_Error: treatment5[i].Vussanut_System_Error[randomarray[i]],
            Vussanut_Model_Error: treatment5[i].Vussanut_Model_Error[randomarray[i]]
        })
    }

    return meemmaseedbest;
}

export const getTreatment6 = () => {
    const randomarray = [];
    for (let i = 0; i < 10; i++) {
        let random = Math.floor(Math.random() * 5);
        randomarray.push(random);
    }

    let meemmaseedbest = [];

    for (let i = 0; i < 10; i++) {
        meemmaseedbest.push({
            Sunshine: treatment6[i].Sunshine[randomarray[i]],
            Temperature: treatment6[i].Temperature[randomarray[i]],
            Wind: treatment6[i].Wind[randomarray[i]],
            Meemmaseed_Optimal_Irrigation: treatment6[i].Meemmaseed_Optimal_Irrigation[randomarray[i]],
            Meemmaseed_System_Error: treatment6[i].Meemmaseed_System_Error[randomarray[i]],
            Meemmaseed_Model_Error: treatment6[i].Meemmaseed_Model_Error[randomarray[i]],
            Vussanut_Optimal_Irrigation: treatment6[i].Vussanut_Optimal_Irrigation[randomarray[i]],
            Vussanut_System_Error: treatment6[i].Vussanut_System_Error[randomarray[i]],
            Vussanut_Model_Error: treatment6[i].Vussanut_Model_Error[randomarray[i]]
        })
    }

    return meemmaseedbest;
}