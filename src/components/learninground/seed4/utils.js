export function generateNums2() {
  return [
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
    {
      x: Math.floor(Math.random() * 18) + 1,
      y: Math.floor(Math.random() * 73) + 32,
      z: Math.floor(Math.random() * 61) + 1,
    },
  ];
}

export function getRandom() {
  var num = Math.random();
  if (num < 0.9) return 1; //probability 0.9
  else return 0; //probability 0.1
}

export function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// Function to generate random number between the range of min and max
function randomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

// Function to return +3 if random number is negative and -3 if random number is positive
function randomerrorincreaser(randomNumber) {
  if (randomNumber < 0) {
    return 3;
  } else {
    return -3;
  }
}

// Optimal Irrigation 
export function generateCorrectData2(nums) {
  return [
    Math.floor(
      
      0.15 * nums[0].x +
      0.55 * nums[0].y -
      0.3 * nums[0].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
     
      0.15 * nums[1].x +
      0.55 * nums[1].y -
      0.3 * nums[1].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[2].x +
      0.55 * nums[2].y -
      0.3 * nums[2].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[3].x +
      0.55 * nums[3].y -
      0.3 * nums[3].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[4].x +
      0.55 * nums[4].y -
      0.3 * nums[4].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
     
      0.15 * nums[5].x +
      0.55 * nums[5].y -
      0.3 * nums[5].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[6].x +
      0.55 * nums[6].y -
      0.3 * nums[6].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[7].x +
      0.55 * nums[7].y -
      0.3 * nums[7].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[8].x +
      0.55 * nums[8].y -
      0.3 * nums[8].z +
      randomNumber(-5, 5)
    ),
    Math.floor(
      
      0.15 * nums[9].x +
      0.55 * nums[9].y -
      0.3 * nums[9].z +
      randomNumber(-5, 5)
    ),
  ];
}

// Vussanut Continous Error (Model Value)
export function generateModelData2(nums) {
  return [
    Math.floor(
     
      0.15 * nums[0].x +
      0.55 * nums[0].y -
      0.3 * nums[0].z 
       
    ),
    Math.floor(
     
      0.15 * nums[1].x +
      0.55 * nums[1].y -
      0.3 * nums[1].z 
    ),
    Math.floor(
      
      0.15 * nums[2].x +
      0.55 * nums[2].y -
      0.3 * nums[2].z  
    ),
    Math.floor(
      
      0.15 * nums[3].x +
      0.55 * nums[3].y -
      0.3 * nums[3].z  
    ),
    Math.floor(
      
      0.15 * nums[4].x +
      0.55 * nums[4].y -
      0.3 * nums[4].z 
    ),
    Math.floor(
      
      0.15 * nums[5].x +
      0.55 * nums[5].y -
      0.3 * nums[5].z  
    ),
    Math.floor(
      
      0.15 * nums[6].x +
      0.55 * nums[6].y -
      0.3 * nums[6].z 
    ),
    Math.floor(
      
      0.15 * nums[7].x +
      0.55 * nums[7].y -
      0.3 * nums[7].z  
    ),
    Math.floor(
      
      0.15 * nums[8].x +
      0.55 * nums[8].y -
      0.3 * nums[8].z  
    ),
    Math.floor(
      
      0.15 * nums[9].x +
      0.55 * nums[9].y -
      0.3 * nums[9].z 
    ),
  ];
}

// function to return random error with a probability 0.9 with a range of -4 to 4 and 0.1 with a range of -30 or 30
export function randProb() {
  var rnd = Math.random();
  if (rnd < 0.9) return randomNumber(-5, 5);
  else if (rnd < 0.95) return 30;
  else return -30;
}

// Vussanut Rare Error
export function generateModelDataRare(nums) {
  return [
    Math.floor(
      40 + 0.15 * nums[0].x + 0.55 * nums[0].y - 0.3 * nums[0].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[1].x + 0.55 * nums[1].y - 0.3 * nums[1].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[2].x + 0.55 * nums[2].y - 0.3 * nums[2].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[3].x + 0.55 * nums[3].y - 0.3 * nums[3].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[4].x + 0.55 * nums[4].y - 0.3 * nums[4].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[5].x + 0.55 * nums[5].y - 0.3 * nums[5].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[6].x + 0.55 * nums[6].y - 0.3 * nums[6].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[7].x + 0.55 * nums[7].y - 0.3 * nums[7].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[8].x + 0.55 * nums[8].y - 0.3 * nums[8].z + randProb()
    ),
    Math.floor(
      40 + 0.15 * nums[9].x + 0.55 * nums[9].y - 0.3 * nums[9].z + randProb()
    ),
  ];
}
