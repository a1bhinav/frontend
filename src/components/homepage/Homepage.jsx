import { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import { Layout, Button } from 'antd'
import 'antd/dist/antd.css'
import './styles.css'

const { Content } = Layout

const Homepage = () => {
  const navigate = useNavigate()
  const location = useLocation();

  const searchParams = new URLSearchParams(location.search);

  const prolificPid = searchParams.get('PROLIFIC_PID');
  const studyId = searchParams.get('STUDY_ID');
  const sessionId = searchParams.get('SESSION_ID');
  const [random, setRandomNumber] = useState();

  // Weighted Random Array Initialization
  const myArray = [6];

  useEffect(() => {
    const newrandom = myArray[Math.floor(Math.random() * myArray.length)];
    if (!random) {
      setRandomNumber(newrandom);
    }
  })

  useEffect(() => {
    async function getUser() {
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {
            Accept: "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
            "Content-Type": "application/json",
          },
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <div className='total-screen'>
      <Layout className='layout'>
        <h1 className='header-style'>Welcome to our task!</h1>
        <Content className='site-layout-content'>
          <div>
            <p className='card-text'>
              All your responses and resulting data will be stored securely and you will remain anonymous.
              <br />
              This task will take approximately <b>15 </b>minutes to complete.
              <br /> You will be paid <b>£1.5</b> for completing the task.
              <br />
              <br />
              Additionally, you can earn a bonus of up to <b>£2.5</b> that depends
              on your actions, as explained in more details on the next pages.
              During the task, we will not speak of Pounds, but Coins.
              <br />
              <br />
              Your entire payoff will first be calculated in Coins. The Coins
              you earn during the task will be converted to Pounds at the end of
              the task.
              <br />
              <br />
              The following conversion rate applies:&nbsp;
              <b>14 Coins = £1</b>
              <br />
              <br />
              Please read through the following instructions carefully. After
              the instructions, you will need to answer a number of
              comprehension questions. Unfortunately, if you cannot answer these
              questions within 2 trials, you cannot participate in this
              experiment.
              <br />
              <br />
              <hr />
              <br />
              If you are certain that you can complete this task without any
              interruption, please click&nbsp;
              <b>Proceed</b> to start with the task. Thank you very much for
              participating!
            </p>
          </div>
          <div className='button-container'>
            <Button
              type='primary'
              shape='round'
              size='large'
              onClick={async () => {
                try {
                  const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/create', {
                    method: 'POST',
                    headers: {
                      Accept: "application/json",
                      "Access-Control-Allow-Origin": "*",
                      "Access-Control-Allow-Methods": "*",
                      "Content-Type": "application/json",
                    },
                    // credentials: 'include',
                    body: JSON.stringify({ prolificPid, studyId, sessionId, random }),
                  })
                  const data = await res.json()

                  if (!(res.status === 201)) {
                    throw new Error(res.err)
                  } else {
                    sessionStorage.setItem('P_ID', data.P_ID)
                    navigate('/instructions')
                  }
                } catch (err) {
                  console.log(err)
                }
              }}
            >
              Proceed
            </Button>
          </div>
        </Content>
      </Layout>
    </div>
  )
}

export default Homepage
