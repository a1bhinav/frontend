import { Layout } from 'antd'
import 'antd/dist/antd.css'

const { Content } = Layout

const Error = () => {

    return (
        <div className='total-screen'>
            <Layout className='layout'>
                <Content className='site-layout-content'>
                    <p className='card-text'>
                  <h1>Unfortunately, you did not answer the comprehension questions correctly. As a result, you are no longer eligible to continue participating in the study.</h1>
                    </p>
                </Content>
            </Layout>
        </div>
    )
}

export default Error
