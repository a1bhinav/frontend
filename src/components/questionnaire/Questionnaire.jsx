import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { Layout, Button, Radio, Slider, Checkbox } from 'antd'
import 'antd/dist/antd.css'
import '../homepage/styles.css'
import "./questionradio.css"

const { Content } = Layout

const CheckboxGroup = Checkbox.Group;

const Question = () => {
  const [page, setPage] = useState(0);
  const currentDate = new Date();
  const fullYear = currentDate.getFullYear();
  const navigate = useNavigate();

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers:  {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  const url = 'https://app.prolific.co/submissions/complete?cc=C17F7LXD';

  const marks = {
    1: 'Not at all willing to take risk',
    10: 'Very willing to take risk',
  }

  const marksNumerical = {
    1: "Not at all good",
    6: "Extremely good"
  }

  const pageSubmit0 = () => {
    if (a1 == null) {
      alert('Please Answer question 1');
    } else if (a2 == null) {
      alert('Please Answer question 2');
    } else if (a3 == null) {
      alert('Please Answer question 3');
    } else if (a4 == null) {
      alert('Please Answer question 4');
    } else {
      setPage(1)
    }
  }

  const pageSubmit1 = () => {
    if (b1 == null) {
      alert('Please Answer question 1');
    } else if (b2 == null) {
      alert('Please Answer question 2');
    } else if (b3 == null) {
      alert('Please Answer question 3');
    } else if (b4 == null) {
      alert('Please Answer question 4');
    } else {
      setPage(2)
    }
  }

  const pageSubmit2 = () => {
    if (c1 == null) {
      alert('Please Answer question 1');
    } else {
      setPage(3)
    }
  }

  const pageSubmit3 = () => {
    if (d1 == null) {
      alert('Please Answer question 1');
    } else if (d2 == null) {
      alert('Please Answer question 2');
    } else if (d3 == null) {
      alert('Please Answer question 3');
    } else if (d4 == null) {
      alert('Please Answer question 4');
    } else if (d5 == null) {
      alert('Please Answer question 5');
    } else if (d6 == null) {
      alert('Please Answer question 6');
    } else if (d7 == null) {
      alert('Please Answer question 7');
    } else if (d8 == null) {
      alert('Please Answer question 8');
    } else {
      setPage(4)
    }
  }

  const pageSubmit4 = () => {
    if (e1 == null) {
      alert('Please Answer question 1');
    } else if (e2 == null) {
      alert('Please Answer question 2');
    } else if (e3 == null) {
      alert('Please Answer question 3');
    } else if (e4 == null) {
      alert('Please Answer question 4');
    } else if (e5 == null) {
      alert('Please Answer question 5');
    } else if (e6 == null) {
      alert('Please Answer question 6');
    } else if (e7 == null) {
      alert('Please Answer question 7');
    } else if (e8 == null) {
      alert('Please Answer question 8');
    } else if (e9 == null) {
      alert('Please Answer question 9');
    } else if (e10 == null) {
      alert('Please Answer question 10');
    } else if (e11 == null) {
      alert('Please Answer question 11');
    } else if (e12 == null) {
      alert('Please Answer question 12');
    } else if (e13 == null) {
      alert('Please Answer question 13');
    } else if (e14 == null) {
      alert('Please Answer question 14');
    } else if (e15 == null) {
      alert('Please Answer question 15');
    } else if (e16 == null) {
      alert('Please Answer question 16');
    } else if (e17 == null) {
      alert('Please Answer question 17');
    } else {
      setPage(5)
    }
  }

  const pageSubmit5 = async () => {
    if (f1 == null) {
      alert('Please Answer question 1');
    } else if (f1 < 1900 || f1 > fullYear) {
      alert('Please check answer 1')
    } else if (f2 == null) {
      alert('Please Answer question 2');
    } else if (f3 == null) {
      alert('Please Answer question 3');
    } else if (f4 == null) {
      alert('Please Answer question 4');
    } else if (f5 == null) {
      alert('Please Answer question 5');
    } else if (f5.length == 0) {
      alert('Please Answer question 5');
    } else {
      const data = { page1: [a1, a2, a3, a4], page2: [b1, b2, b3, b4], page3: c1, page4: [d1, d2, d3, d4, d5, d6, d7, d8], page5: [e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17], page6: [f1, f2, f3, f4, f5] }
      try {
        const P_ID = await sessionStorage.getItem("P_ID");

        const res = await fetch("https://human-computer-interaction-backend.onrender.com/api/questionData", {
          method: "POST",
          headers:  {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          },  
          body: JSON.stringify({
            P_ID: P_ID, QuestionData: data
          }),
          //credentials: "include",
        });
        if (!(res.status === 200)) {
          alert("Some Error Occured");
        } else {
          const link = document.createElement('a');
          link.href = url;
          link.click();
        }
      } catch (err) {
        console.log(err);
      }
    }
  }

  const [a1, seta1] = useState(null);
  const [a2, seta2] = useState(null);
  const [a3, seta3] = useState(null);
  const [a4, seta4] = useState(null);

  const [b1, setb1] = useState(null);
  const [b2, setb2] = useState(null);
  const [b3, setb3] = useState(null);
  const [b4, setb4] = useState(null);

  const [c1, setc1] = useState(5);

  const [d1, setd1] = useState(null);
  const [d2, setd2] = useState(null);
  const [d3, setd3] = useState(null);
  const [d4, setd4] = useState(null);
  const [d5, setd5] = useState(null);
  const [d6, setd6] = useState(null);
  const [d7, setd7] = useState(null);
  const [d8, setd8] = useState(null);

  const [e1, sete1] = useState(null);
  const [e2, sete2] = useState(null);
  const [e3, sete3] = useState(null);
  const [e4, sete4] = useState(null);
  const [e5, sete5] = useState(null);
  const [e6, sete6] = useState(null);
  const [e7, sete7] = useState(null);
  const [e8, sete8] = useState(null);
  const [e9, sete9] = useState(null);
  const [e10, sete10] = useState(null);
  const [e11, sete11] = useState(null);
  const [e12, sete12] = useState(null);
  const [e13, sete13] = useState(null);
  const [e14, sete14] = useState(null);
  const [e15, sete15] = useState(null);
  const [e16, sete16] = useState(null);
  const [e17, sete17] = useState(null);

  const [f1, setf1] = useState(null);
  const [f2, setf2] = useState(null);
  const [f3, setf3] = useState(null);
  const [f4, setf4] = useState(null);
  const [f5, setf5] = useState(null);

  const handleChange = (val) => {
    setc1(val) // Update the state with the slider value
  }

  const options = ["None", "A little", "Some", "A Fair Amount", "A Lot"];

  const whatIsYourSex = ["Male", "Female", "Non-binary", "Prefer not to say"];

  const employementStatus = [
    "Working (paid employee)",
    "Working (self-employed)",
    "Not working (temporary layoff from a job)",
    "Not working (looking for work)",
    "Not working (retired)",
    "Not working (disabled)",
    "Not working (other)",
    "Prefer not to answer",
  ];

  const educationLevel = [
    "Less than high school degree",
    "High school graduate (high school diploma or equivalent including GED)",
    "Some college but no degree",
    "Associate degree in college (2-year)",
    "Bachelor's degree in college (4-year)",
    "Master's degree",
    "Doctoral degree",
    "Professional degree (JD, MD)",
  ]

  const ethinicity = [
    "African American",
    "American Indian",
    "Asian",
    "Hispanic/Latino",
    "White/Caucasian",
    "Other",
  ]

  const optionsNumeric = ['1', '2', '3', '4', '5', '0'];

  return (
    <div className='total-screen'>
      <Layout className='layout'>
        <h1 className='header-style'>Questionnaire</h1>
        <Content className='site-layout-content'>
          {page === 0 &&
            <>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 1</p>
                <p>How much confidence do you have in the <b><u>AI System’s predictions</u></b>  for optimal Meemmaseed irrigation</p>
                <Radio.Group
                  options={options.map((option) => option)}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                  }}
                  required
                  value={a1}
                  onChange={(e) => {
                    let answer = e.target.value;
                    seta1(answer)
                  }}
                ></Radio.Group>
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 2</p>
                <p>How much confidence do you have in <b><u>your</u></b> predictions for optimal Meemmaseed irrigation?</p>
                <Radio.Group
                  options={options.map((option) => option)}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                  }}
                  required
                  value={a2}
                  onChange={(e) => {
                    let answer = e.target.value;
                    seta2(answer)
                  }}
                ></Radio.Group>
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 3</p>
                <p>How many units do you think the <b><u>AI System’s predictions</u></b> are off by for Meemmaseed on average?</p>
                <p>Please enter a number between 0 - 100</p>
                <input
                  type='number'
                  required
                  min={0}
                  max={100}
                  style={{ width: '20vw', maxWidth: '200px' }}
                  value={a3}
                  onChange={e => {
                    let value = e.target.value;
                    if (value < 0) {
                      value = 0;
                      seta3(0);
                    } else if (value > 100) {
                      value = 100;
                      seta3(100);
                    } else {
                      seta3(value);
                    }
                  }}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 4</p>
                <p>How many units do you think <b><u>your</u></b> predictions are off by for Meemmaseed on average?</p>
                <p>Please enter a number between 0 - 100</p>
                <input
                  type='number'
                  required
                  min={0}
                  max={100}
                  style={{ width: '20vw', maxWidth: '200px' }}
                  value={a4}
                  onChange={e => {
                    let value = e.target.value;
                    if (value < 0) {
                      value = 0;
                      seta4(0);
                    } else if (value > 100) {
                      value = 100;
                      seta4(100);
                    } else {
                      seta4(value);
                    }
                  }}
                />
              </div>
              <div className="button-container">
                <Button type="primary" shape="round" size="large" onClick={pageSubmit0}>
                  Next
                </Button>
              </div>
            </>}
          {page === 1 &&
            <>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 1</p>
                <p>How much confidence do you have in the <b><u>AI System’s predictions</u></b>  for optimal Vussanut irrigation</p>
                <Radio.Group
                  options={options.map((option) => option)}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                  }}
                  required
                  value={b1}
                  onChange={(e) => {
                    let answer = e.target.value;
                    setb1(answer)
                  }}
                ></Radio.Group>
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 2</p>
                <p>How much confidence do you have in <b><u>your</u></b> predictions for optimal Vussanut irrigation?</p>
                <Radio.Group
                  options={options.map((option) => option)}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                  }}
                  required
                  value={b2}
                  onChange={(e) => {
                    let answer = e.target.value;
                    setb2(answer)
                  }}
                ></Radio.Group>
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 3</p>
                <p>How many units do you think the <b><u>AI System’s predictions</u></b> are off by for Vussanut on average?</p>
                <p>Please enter a number between 0 - 100</p>
                <input
                  type='number'
                  required
                  min={0}
                  max={100}
                  style={{ width: '20vw', maxWidth: '200px' }}
                  value={b3}
                  onChange={e => {
                    let value = e.target.value;
                    if (value < 0) {
                      value = 0;
                      setb3(0);
                    } else if (value > 100) {
                      value = 100;
                      setb3(100);
                    } else {
                      setb3(value);
                    }
                  }}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 4</p>
                <p>How many units do you think <b><u>your</u></b> predictions are off by for Vussanut on average?</p>
                <p>Please enter a number between 0 - 100</p>
                <input
                  type='number'
                  required
                  min={0}
                  max={100}
                  style={{ width: '20vw', maxWidth: '200px' }}
                  value={b4}
                  onChange={e => {
                    let value = e.target.value;
                    if (value < 0) {
                      value = 0;
                      setb4(0);
                    } else if (value > 100) {
                      value = 100;
                      setb4(100);
                    } else {
                      setb4(value);
                    }
                  }}
                />
              </div>
              <div className="button-container">
                <Button type="primary" shape="round" size="large" onClick={pageSubmit1}>
                  Next
                </Button>
              </div>
            </>}
          {page === 2 &&
            <>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>
                  How do you see yourself: are you generally a person who is fully
                  prepared to take risks or do you try to avoid taking risks?
                </p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={10}
                  value={c1}
                  onChange={handleChange}
                  marks={marks}
                />
              </div>
              <div className="button-container">
                <Button type="primary" shape="round" size="large" onClick={pageSubmit2}>
                  Next
                </Button>
              </div>
            </>}
          {page === 3 &&
            <>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 1</p>
                <p>How good are you at working with fractions?</p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d1}
                  onChange={(val) => { setd1(val) }}
                  marks={marksNumerical}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 2</p>
                <p>How good are you at working with percentages?</p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d2}
                  onChange={(val) => { setd2(val) }}
                  marks={marksNumerical}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 3</p>
                <p>How good are you at calculating a 15% tip?</p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d3}
                  onChange={(val) => { setd3(val) }}
                  marks={marksNumerical}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 4</p>
                <p>How good are you at figuring out how much a shirt will cost if it is 25% off?</p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d4}
                  onChange={(val) => { setd4(val) }}
                  marks={marksNumerical}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 5</p>
                <p>When reading the newspaper, how <b>helpful</b> do you find tables and graphs that are parts of a
                  story?</p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d5}
                  onChange={(val) => { setd5(val) }}
                  marks={{ 1: "Not at all helpful", 6: 'Extremely helpful' }}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 6</p>
                <p>When people tell you the chance of something happening, do you prefer that they use
                  <b> words</b> ("it rarely happens") or <b>numbers</b> ("there's a 1% chance")? </p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d6}
                  onChange={(val) => { setd6(val) }}
                  marks={{ 1: "Always Prefer Words", 6: "Always Prefer Numbers" }}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 7</p>
                <p>When you hear a weather forecast, do you prefer predictions using <b>percentages</b> (e.g.,
                  “there will be a 20% chance of rain today”) or predictions using only <b>words</b> (e.g., “there is a
                  small chance of rain today”)? </p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d7}
                  onChange={(val) => { setd7(val) }}
                  marks={{ 1: "Always Prefer Percentages", 6: "Always Prefer Words" }}
                />
              </div>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                }}
              >
                <p>Q 8</p>
                <p>How <b>often</b> do you find numerical information to be useful?</p>
                <Slider
                  style={{ maxWidth: '500px', margin: '40px auto' }}
                  min={1}
                  max={6}
                  value={d8}
                  onChange={(val) => { setd8(val) }}
                  marks={{ 1: "Never", 6: 'Very Often' }}
                />
              </div>
              <div className="button-container">
                <Button type="primary" shape="round" size="large" onClick={pageSubmit3}>
                  Next
                </Button>
              </div>
            </>}
          {page === 4 &&
            <>
              <div
                className='card-text'
                style={{
                  border: '1px solid grey',
                  borderRadius: '8px',
                  margin: '24px auto',
                  padding: '32px',
                  // display: "flex"
                }}
              >
                <p style={{ margin: 0, textAlign: 'center' }}>Please respond to the following based on your experience with the AI system in the tasks you just completed</p>
                <table className='radio' style={{ width: '100%' }}>
                  <tr>
                    <td></td>
                    <td width="45%"></td>
                    <td width="45%">
                      <div style={{ display: 'flex', gap: "12px", textAlign: "center", alignItems: "center", justifyContent: 'center', width: '100%', margin: '0', fontWeight: "bold", fontSize: "14px" }}>
                        <span>Strongly disagree</span>
                        <span>Rather disagree</span>
                        <span>Neither disagree nor agree</span>
                        <span>Rather agree</span>
                        <span>Strongly agree</span>
                        <span>No response</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>The system is capable of interpreting situations correctly.</td>
                    <td className='radio'>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e1}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete1(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>The system state was always clear to me.</td>
                    <td className='radio'>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e2}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete2(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>I already know similar systems.</td>
                    <td className='radio'>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e3}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete3(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>One should be careful with unfamiliar automated systems.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e4}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete4(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>The system works reliably.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e5}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete5(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>6</td>
                    <td>The system reacts unpredictably</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e6}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete6(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>7</td>
                    <td>I trust the system.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e7}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete7(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>8</td>
                    <td>A system malfunction is likely</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e8}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete8(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>9</td>
                    <td>I was able to understand why things happened.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e9}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete9(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>10</td>
                    <td>I rather trust a system than I mistrust it</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e10}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete10(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>11</td>
                    <td>The system is capable of taking over complicated tasks.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e11}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete11(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>12</td>
                    <td>I can rely on the system.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e12}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete12(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>13</td>
                    <td>The system might make sporadic errors.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e13}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete13(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>14</td>
                    <td>It’s difficult to identify what the system will do next.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e14}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete14(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>I have already used similar systems. </td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e15}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete15(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>16</td>
                    <td>Automated systems generally work well.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e16}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete16(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                  <tr>
                    <td>17</td>
                    <td>I am confident about the system’s capabilities.</td>
                    <td>
                      <Radio.Group
                        options={optionsNumeric.map((option) => option == "0" ? "" : option)}
                        style={{
                          display: 'flex', flexDirection: "row",
                          justifyContent: 'space-around',
                        }}
                        required
                        value={e17}
                        onChange={(e) => {
                          let answer = e.target.value;
                          sete17(answer)
                        }}
                      >
                      </Radio.Group>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="button-container">
                <Button type="primary" shape="round" size="large" onClick={pageSubmit4}>
                  Next
                </Button>
              </div>
            </>}
          {page === 5 && <>
            <div
              className='card-text'
              style={{
                border: '1px solid grey',
                borderRadius: '8px',
                margin: '24px auto',
                padding: '32px',
              }}
            >
              <div style={{ display: 'flex' }}>
                <p style={{ marginRight: "16px" }}>Q 1</p>
                <p>What is your year of birth?</p>
              </div>
              <input
                type='number'
                required
                min={1900}
                max={fullYear}
                style={{ width: '20vw', maxWidth: '200px' }}
                value={f1}
                onChange={e => {
                  let value = e.target.value;
                  if (value < 0) {
                    value = 0;
                    setf1(0);
                  } else if (value > (new Date().getFullYear())) {
                    value = (new Date().getFullYear());
                    setf1(value);
                  } else {
                    setf1(value);
                  }
                }}
              />
            </div>
            <div
              className='card-text'
              style={{
                border: '1px solid grey',
                borderRadius: '8px',
                margin: '24px auto',
                padding: '32px',
              }}
            >
              <div style={{ display: "flex" }}>
                <p style={{ marginRight: '16px' }}>Q 2</p>
                <p>What is your gender?</p>
              </div>
              <Radio.Group
                options={whatIsYourSex.map((option) => option)}
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                }}
                required
                value={f2}
                onChange={(e) => {
                  let answer = e.target.value;
                  setf2(answer)
                }}
              ></Radio.Group>
            </div>
            <div
              className='card-text'
              style={{
                border: '1px solid grey',
                borderRadius: '8px',
                margin: '24px auto',
                padding: '32px',
              }}
            >
              <div style={{ display: "flex" }}>
                <p style={{ marginRight: '16px' }}>Q 3</p>
                <p>What is your employment status?</p>
              </div>
              <Radio.Group
                options={employementStatus.map((option) => option)}
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                }}
                required
                value={f3}
                onChange={(e) => {
                  let answer = e.target.value;
                  setf3(answer)
                }}
              ></Radio.Group>
            </div>
            <div
              className='card-text'
              style={{
                border: '1px solid grey',
                borderRadius: '8px',
                margin: '24px auto',
                padding: '32px',
              }}
            >
              <div style={{ display: "flex" }}>
                <p style={{ marginRight: '16px' }}>Q 4</p>
                <p>What is your highest level of education?</p>
              </div>
              <Radio.Group
                options={educationLevel.map((option) => option)}
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                }}
                required
                value={f4}
                onChange={(e) => {
                  let answer = e.target.value;
                  setf4(answer)
                }}
              ></Radio.Group>
            </div>
            <div
              className='card-text'
              style={{
                border: '1px solid grey',
                borderRadius: '8px',
                margin: '24px auto',
                padding: '32px',
              }}
            >
              <div style={{ display: "flex" }}>
                <p style={{ marginRight: '16px' }}>Q 5</p>
                <p>What is your ethnicity?</p>
              </div>
              <div className='checkbox'>
                <CheckboxGroup options={ethinicity} value={f5} onChange={e => setf5(e)} />
              </div>
            </div>
            <div className="button-container">
              <Button type="primary" shape="round" size="large" onClick={pageSubmit5}>
                Submit
              </Button>
            </div>
          </>}
        </Content>
      </Layout>
    </div>
  )
}

export default Question