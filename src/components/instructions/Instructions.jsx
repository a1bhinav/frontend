import React, { useState, useEffect } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { Layout, Button } from 'antd'

import 'antd/dist/antd.css'

const { Content } = Layout

const Instructions = () => {
  const [page, setPage] = useState(0)
  const currentPage = [0, 1, 2, 3, 4, 5, 6, 7];
  const navigate = useNavigate();

  useEffect(() => {
    async function getUser() {
      const prolificPid = sessionStorage.getItem("P_ID");
      try {
        const res = await fetch('https://human-computer-interaction-backend.onrender.com/api/getUser', {
          method: 'POST',
          headers: {  
            Accept: "application/json",  
            "Access-Control-Allow-Origin": "*",  
            "Access-Control-Allow-Methods": "*",  
            "Content-Type": "application/json",  
          }, 
          //credentials: 'include',
          body: JSON.stringify({ pid: prolificPid }),
        })
        const data = await res.json()

        if (!(res.status === 201)) {
          throw new Error(res.err)
        } else {
          if (data.kickedOut != false) {
            navigate('/error')
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    getUser();
  }, [])

  return (
    <div className='total-screen'>
      <Layout className='layout'>
        <h1 className='header-style'>Instructions</h1>
        <Content className='site-layout-content'>
          <div>
            <p className='card-text'>
              <div>
                {page === 0 && (
                  <p>
                    In this task, you will take on the role of a <b>farmer</b>.
                    Your farm grows
                    <b> two different crops</b>, each consuming <b>1 hectare</b>{' '}
                    of land. The
                    <b> more</b> crops your farm produces, the <b> higher</b>{' '}
                    your additional bonus payoff.
                    <br /> The two crop types are:
                    <br />
                    <table
                      style={{ margin: '1% 0 1% 0', width: '20%' }}
                      className='text-center'
                    >
                      <tr>
                        <th style={{ width: '10%' }}>Meemmaseed</th>
                        <th style={{ width: '10%' }}>Vussanut</th>
                      </tr>
                    </table>
                    Your job in this task is to oversee the
                    <b> irrigation of your crops</b>.<br />
                    Specifically, you will decide with how many thousand gallons
                    of water you want to water the different crop types.
                    <br />
                    To make an informed decision, you will receive information
                    about
                    <b> three environmental factors</b> that determine
                    irrigation need:
                    <br />
                    <br />
                    <table
                      style={{ margin: 0 }}
                      className='text-center'
                    >
                      <tr>
                        <th>Sunshine (hours/day)</th>
                        <th>Average day temperature (Fahrenheit)</th>
                        <th>Wind speed (km/h)</th>
                      </tr>
                    </table>
                    <br />
                    Use the numbered buttons for navigation at the bottom of
                    this page, you can also click on <b>&gt;&gt;</b> to go to
                    the next page and <b>&lt;&lt;</b> to go to the previous
                    page.
                  </p>
                )}

                {page === 1 && (
                  <p>
                    You know that, under ideal conditions, each of your crops
                    needs <b> at least forty thousand gallons of water</b>.
                    However, usually, they need more. Therefore, each round, you
                    need to predict the{' '}
                    <b> additional amount of irrigation </b> your crops need for
                    a given set of environment inputs.
                    <br />
                    <br />
                    The <b>three environmental input factors</b> are the main
                    determinants for the <b>optimal amount of irrigation</b>,
                    although there may be some
                    <b> randomness</b> involved.
                    <br />
                    <br /> The <b>effect of the environment</b> on optimal
                    irrigation <b>does not change over time</b>. Thus, the
                    process by which the environmental factors determine
                    irrigation for each crop type always remains the same.
                    <br />
                    <br />{' '}
                    <p>
                      <b>
                        Additionally, the two crops respond differently to the three environmental factors
                      </b>
                      . Thus, knowing the impact of sunshine hours on Meemmaseed irrigation,
                      for example, does not necessarily tell you much about its impact on Vussanut.
                    </p>
                  </p>
                )}

                {page === 2 && (
                  <p>
                    <b>Task procedure:</b>
                    <br />
                    This task has two stages: the
                    <b> Training Phase </b>
                    and the <b>Official Rounds</b>.<br />
                    <br /> During the <b>Training Phase</b>, you will be able to{' '}
                    <b>familiarize yourself with the irrigation requirements</b>{' '}
                    of your two crops. You will receive two types of
                    information:
                    <br />
                    <br />
                    (1) You will observe descriptive information about the
                    relationship between your crops and the environment for{' '}
                    <b>20 </b>
                    simulated rounds.
                    <br />
                    (2) You will complete <b>10 Training Rounds</b>. In these
                    training rounds, you will be asked to predict how much you
                    water each of your crops need, given the environmental
                    variables.
                    <br />
                    <br />
                    <b>
                      Your predictions during the Training Phase do not affect
                      your bonus payoff
                    </b>
                    .
                  </p>
                )}

                {page === 3 && (
                  <p>
                    <b>Task procedure:</b>
                    <br />
                    During the <b>Training Rounds</b>, you will also observe the{' '}
                    <b>irrigation predictions of an AI System</b>. The AI System predicts each crop’s irrigation need simultaneously.
                    <br />
                    <br />
                    You can observe the following information:
                    <br />
                    &emsp;(1) The AI System’s accuracy during the 20 simulated training rounds for both crops
                    <br />
                    &emsp;(2) A graphical representation of the AI System’s accuracy for the 20 simulated training rounds
                    <br />
                    &emsp;(3) Feedback about the AI System’s irrigation prediction after each of your own irrigation predictions during the 10 training rounds
                    <br />
                    <br /> The AI System uses the same information that you will
                    receive. It does not receive any{' '}
                    <b>additional information</b> that you will not receive.
                  </p>
                )}

                {page === 4 && (
                  <p>
                    <b>Task procedure:</b>
                    <br />
                    At the end of each training round, you will see:
                    <div>
                      <ul>
                        1.{' '}
                        <b>
                          Your additional irrigation predictions for both crops
                        </b>
                      </ul>
                      <ul>
                        2.{' '}
                        <b>
                          The optimal additional amount of irrigation for both
                          crops
                        </b>
                      </ul>
                      <ul>
                        3.{' '}
                        <b>
                          The AI System’s additional irrigation prediction for both crops
                        </b>
                      </ul>
                    </div>
                    Remember that in each of the 10 training rounds, you make a
                    prediction for both of your crops. After you have completed
                    the 10 training rounds and familiarized yourself with the
                    descriptive information, you will proceed to the Official
                    Rounds.
                  </p>
                )}

                {page === 5 && (
                  <p>
                    <p style={{ textAlign: 'left', marginBottom: 0 }}>
                      <b>The Official Rounds:</b>
                      <br />
                    </p>
                    During the Official Rounds, you will make irrigation
                    predictions about the two crops that determine your additional
                    bonus payoff from this task.
                    <br />
                    <br />
                    You will complete 10 Official Rounds. In each Official
                    Round, you will make an irrigation prediction for both of your
                    crops simultaneously.
                    <br />
                    <br />
                    Afterwards, you will choose for each crop between using your own irrigation prediction,
                    or the prediction of the AI System.
                    <br />
                    <br />
                    If you choose your own prediction, your decision will be the
                    one determining your income from this task. If you choose
                    the AI System’s prediction, your income will be
                    determined by the accuracy of the AI System’s irrigation
                    prediction.
                  </p>
                )}

                {page === 6 && (
                  <p>
                    <b>Payment Official Rounds: </b>
                    <br />
                    Your accuracy during the Official Rounds determines your
                    bonus income from this task. Your bonus will depend on how
                    close your irrigation predictions are to the optimal
                    additional irrigation as determined by the environment. Because every Official Round
                    could be the one round that determines your income, one good prediction does not necessarily offset one bad prediction.
                    <br />
                    <br />
                    Every round, the bonus will be determined as follows:
                    <br /> <b>Bonus = 35 Coins – Irrigation Error</b>
                    <br /> If you predict the perfect additional irrigation
                    amount, you receive 35 Coins. For each point your implemented prediction
                    (either your own or the AI System’s) is off by, your bonus decreases by 1 Coin.
                    <br />
                    <br />
                    <b>Example:</b>
                    <br />
                    Optimal Amount of Additional Irrigation (in thousand
                    gallons): 26 <br />
                    <br />
                    Your Prediction: 16 <br />
                    Your Irrigation Error: 10 <br />
                    <br />
                    The AI System’s  Prediction: 19 <br />
                    The AI System’s  Irrigation Error: 7 <br />
                    <br />
                    In this example, if you chose to rely on your prediction,
                    you would receive a bonus of 25 Coins (35 - Your Irrigation
                    Error). If you chose to rely on the AI System’s prediction, you
                    would receive a bonus of 28 Coins (35 - The AI System’s
                    Irrigation Error).<br /><br />
                    <b>Note:</b> This is merely an example, it is possible that your
                    predictions are more accurate than those of the AI System and lead to higher bonuses.
                  </p>
                )}

                {page === 7 && (
                  <>
                    <p>
                      Click on the button below to proceed to the comprehension questions. Once you answer them correctly,
                      you will proceed to the Training Phase.
                      Use the information and the training rounds to familiarize yourself with the task, your crops and the AI System.
                      <br />
                      <br />
                      <p>
                        <b>Note: </b>
                        <b>
                          If you cannot answer all comprehension questions
                          within two trials, you will not be able to participate
                          in this task.
                        </b>
                      </p>
                    </p>
                    <div
                      className='button-container'
                      style={{ marginTop: '10%' }}
                    >
                      <Button type='primary' shape='round' size='large'
                        onClick={async () => {
                          try {
                            const P_ID = await sessionStorage.getItem("P_ID");

                            const res = await fetch("https://human-computer-interaction-backend.onrender.com/api/isuservalid", {
                              method: "POST",
                              headers: {  
                            Accept: "application/json",  
                            "Access-Control-Allow-Origin": "*",  
                            "Access-Control-Allow-Methods": "*",  
                            "Content-Type": "application/json",  
                          }, 
                              body: JSON.stringify({
                                P_ID,
                              }),
                             // credentials: "include",
                            });
                            if (!(res.status === 200)) {
                              alert("You are not allowed");
                              navigate('/error');
                            } else {
                              navigate("/instructionquestion");
                            }
                          } catch (err) {
                            console.log(err);
                          }
                        }}>
                        Proceed to the comprehension questions
                      </Button>
                    </div>
                  </>
                )}
              </div>
            </p>
          </div>
          <div id='wrapper'>
            <div class='b-pagination-outer'>
              <ul id='border-pagination'>
                <li>
                  <button
                    onClick={() => {
                      if (page !== 0) {
                        setPage(page - 1)
                      }
                    }}
                  >
                    «
                  </button>
                </li>

                {currentPage.map((currentPage) => (
                  <li key={currentPage}>
                    {currentPage === page && (
                      <button
                        class='active'
                        onClick={() => {
                          setPage(currentPage)
                        }}
                      >
                        {currentPage + 1}
                      </button>
                    )}
                    {currentPage !== page && (
                      <button
                        onClick={() => {
                          setPage(currentPage)
                        }}
                      >
                        {currentPage + 1}
                      </button>
                    )}
                  </li>
                ))}
                <li>
                  <button
                    onClick={() => {
                      if (page !== 7) {
                        setPage(page + 1)
                      }
                    }}
                  >
                    »
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </Content>
      </Layout>
    </div>
  )
}

export default Instructions
