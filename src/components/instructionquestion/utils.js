export function generateQ1() {
  return {
    question: "In this task you will",
    options: [
      "make blind predictions on the optimal irrigation amount for 2 different crops",
      "make predictions on the optimal irrigation amount for 2 different crops based on three environmental factors",
      "make predictions on the optimal irrigation amount for 1 crop",
      "program an AI System",
    ],
    answer: -1,
  };
}

export function generateQ2() {
  return {
    question: "The optimal irrigation amount for each crop type",
    options: [
      "always follows the same process based on three environmental inputs",
      "is determined by a random draw",
      "is based on more than three environmental input variables",
      "changes over the course of this task",
    ],
    answer: -1,
  };
}

export function generateQ3() {
  return {
    question: "During the Training Rounds you can",
    options: [
      "earn bonus money",
      "not do anything",
      "choose between yourself and the AI System",
      "learn about the task, the crop types and the AI System",
    ],
    answer: -1,
  };
}

export function generateQ4() {
  return {
    question: "During the Official Rounds you",
    options: [
      "choose between your own predictions and the AI System's predictions",
      "do not have to make your own predictions",
      "cannot earn any money",
      "make predictions for one of the two crop types",
    ],
    answer: -1,
  };
}
