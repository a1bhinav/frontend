import React from "react";
import { NavLink } from "react-router-dom";

import { Layout, Button } from "antd";
import "antd/dist/antd.css";

const { Content } = Layout;

const TransitionScreen = () => {
  return (
    <div className="total-screen">
      <Layout className="layout">
        <h1 className="header-style">Transition Training Round</h1>
        <Content className="site-layout-content">
          <div>
            <p className="card-text">
              You have successfully completed the Training Phase. You will now continue to the Official Rounds.
              The Official Rounds determine your bonus income. You will make 10 predictions for both crops. For
              each prediction, you can choose to rely on yourself or the AI System’s prediction. Each point your
              implemented prediction is off by reduces your bonus income for that round by 1 Coin.
            </p>
          </div>
          <div className="button-container">
            <NavLink to="/learninground">
              <Button type="primary" shape="round" size="large">
                Start Official Rounds
              </Button>
            </NavLink>
          </div>
        </Content>
      </Layout>
    </div>
  );
};

export default TransitionScreen;
